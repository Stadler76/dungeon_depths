﻿Public Class Shopkeeper
    Inherits ShopNPC
    Sub New()
        MyBase.New()

        '|ID Info|
        name = "Shopkeeper"
        sName = name
        npc_index = sNPCInd.shopkeeper

        '|NPC Flags|
        pronoun = "he"
        p_pronoun = "his"
        r_pronoun = "him"
        isShop = True

        '|Inventory|
        inv.setCount("Compass", 1)
        inv.setCount("Spellbook", 1)
        inv.setCount("Major_Health_Potion", 1)
        inv.setCount("Anti_Curse_Tag", 1)
        inv.item("Anti_Curse_Tag").value *= 2.5
        'Potions
        inv.setCount("Health_Potion", 1)
        inv.setCount("Mana_Potion", 1)
        inv.setCount("Anti_Venom", 1)
        'Food
        inv.setCount("Chicken_Leg", 1)
        'Armor/Accesories
        inv.setCount("Bronze_Armor", 1)
        inv.setCount("Steel_Armor", 1)
        'Weapons
        inv.setCount("Steel_Sword", 1)
        inv.setCount("Oak_Staff", 1)
        inv.setCount("Gold_Sword", 1)
        inv.setCount("Golden_Staff", 1)
        If DDDateTime.isSummer Then inv.setCount("Staff_of_the_Tidemage", 1)

        '|Stats|
        maxHealth = 9999
        attack = 99
        defense = 999
        speed = 99
        will = 999
        gold = 99999
        xp_value = (maxHealth + attack + defense + speed) / 4
        sMaxHealth = maxHealth
        sMaxMana = maxMana
        sAttack = attack
        sdefense = defense
        sWill = will
        sSpeed = speed

        '|Images|
        picNormal = ShopNPC.npcLib.atrs(0).getAt(0)
        picPrincess = ShopNPC.npcLib.atrs(0).getAt(2)
        picBunny = ShopNPC.npcLib.atrs(0).getAt(1)
        picArachne = ShopNPC.npcLib.atrs(0).getAt(67)

        picNPC = New List(Of Image)
        picNPC.AddRange({picNormal, ShopNPC.npcLib.atrs(0).getAt(4), ShopNPC.npcLib.atrs(0).getAt(5), picPrincess, picBunny})

        picNPC.AddRange({ShopNPC.npcLib.atrs(0).getAt(3),
                         picArachne,
                         ShopNPC.npcLib.atrs(0).getAt(89),
                         ShopNPC.npcLib.atrs(0).getAt(96)})
    End Sub

    Public Overrides Sub encounter()
        MyBase.encounter()

        MyBase.discount = 0

        If Game.mDun.numCurrFloor < 2 Then
            inv.setCount("Scale_Armor", 1)
            inv.setCount("Gold_Armor", 0)
            inv.setCount("Midas_Gauntlet", 0)
        Else
            inv.setCount("Scale_Armor", 1)
            inv.setCount("Gold_Armor", 1)
            inv.setCount("Midas_Gauntlet", 1)
        End If

        If Game.player1.quests(qInds.helpWanted).getComplete Then
            inv.setCount("Platinum_Axe", 1)
            inv.setCount("Platinum_Daggers", 1)
            inv.setCount("Platinum_Staff", 1)
            inv.setCount("Platinum_Armor", 1)
            inv.setCount("Crimson_Cloak", 1)
            inv.setCount("Oak_Staff", 0)
            inv.setCount("Steel_Sword", 0)
            inv.setCount("Bronze_Armor", 0)
            inv.setCount("Steel_Armor", 0)
        End If

        If img_index = 0 Then
            If Game.player1.quests(qInds.helpWanted).canGet Then Game.player1.quests(qInds.helpWanted).init() : Exit Sub
            TextEvent.pushNPCDialog("Hey, what's up?")
        ElseIf img_index = 6 Then
            TextEvent.pushNPCDialog("Hey, what's up?")
        ElseIf img_index = 1 Then
            TextEvent.pushNPCDialog("Ribbit.  Ribbit.")
        ElseIf img_index = 2 Then
            TextEvent.pushNPCDialog("Baaahhh.")
        ElseIf img_index = 3 Then
            TextEvent.pushNPCDialog("Hello, kind " & Game.player1.className & ", how are you on this fine day?")
        ElseIf img_index = 4 Or img_index = 7 Then
            TextEvent.pushNPCDialog("*giggle* Hey!")
        ElseIf img_index = 5 Or img_index = 8 Then
            TextEvent.pushNPCDialog("...")
        End If

        If img_index = getArachneImageInd() And Not Game.player1.formName.Equals("Arachne") Then inv.setCount(244, 1) Else inv.setCount(244, 0)

    End Sub
    Public Overrides Function toFight() As String
        If img_index = 0 Or img_index = 6 Then
            Return "So you want to fight, eh?  I'm ready whenever you are."
        ElseIf img_index = 1 Then
            Return "Ribbit . . ."
        ElseIf img_index = 2 Then
            Return "BAAAAAHHHH!"
        ElseIf img_index = 3 Then
            Return "You would dare to challenge me? If you wish to die, you could just say so."
        ElseIf img_index = 4 Or img_index = 7 Then
            Return "I might not be the best fighter any more, but I can definitely give it my best!"
        ElseIf img_index = 5 Or img_index = 8 Then
            Return "..."
        End If
        Return "Bad move."
    End Function
    Public Overrides Function hitBySpell() As String
        If img_index = 0 Or img_index = 6 Then
            Game.NPCtoCombat(Me)
            Return "Did . . . did you just cast a spell on me?  You know I have to kill you now, right?"
        ElseIf img_index = 1 Then
            Game.NPCtoCombat(Me)
            Return "Ribbit!!!"
        ElseIf img_index = 2 Then
            Game.NPCtoCombat(Me)
            Return "[angry bleets]!"
        ElseIf img_index = 3 Then
            Game.NPCtoCombat(Me)
            Return "Casting spells on royalty is genrally not a good idea."
        ElseIf img_index = 4 Or img_index = 7 Then
            Return "*giggle* Was that magic?"
        ElseIf img_index = 5 Or img_index = 8 Then
            Return "..."
        End If
        Return "Woah there!"
    End Function

    Public Overrides Function reactToSpell(spell As String) As Boolean
        If Rnd() < (0.01) Then
            Return True
        Else
            TextEvent.pushLog("The spell bounces off the Shopkeeper!")
            TextEvent.pushCombat("The spell bounces off the Shopkeeper!")
            Return False
        End If
    End Function

    Public Overrides Sub playerDeath(ByRef p As Player)
        Game.fromCombat()
        p.petrify(Color.Goldenrod, 9999)

        Dim out As String = """You should have known better than to try and rob a shop keeper,"" the shopkeep says, glaring down at you, ""...and if its gold you're after, I guess I've got some good news for you.""" & DDUtils.RNRN &
            "With that, " & pronoun & " reaches into " & p_pronoun & " bag and puts on a gaudy gauntlet that begins glowing with a golden light. You lack the strength to fight back as " & pronoun & " places" &
            " his thumb on your forhead, and suddenly everything just seems so heavy. ""Noooo..."" you moan, as the area around where he touched turns to gold, and that gold turns your flesh and blood " &
            "around it to gold as well. In a matter of seconds, all that is left of " & p.name & " the " & p.className & " is a solid gold statue. The shopkeeper sighs, muttering to no one in particular, " & DDUtils.RNRN &
            """Now how am I going to get you back to the refinery?""" & DDUtils.RNRN &
            "GAME OVER!"

        TextEvent.push(out, AddressOf p.die)
        p.changeClass("Trophy")
    End Sub
End Class
