﻿Public Class Inventory
    Dim internal_inventory As New Dictionary(Of String, Item)
    Dim armor() As Armor
    Dim weapons() As Weapon
    Dim acce() As Accessory
    Dim glasses() As Glasses
    Dim useable(), food(), potions(), misc() As Item
    Dim mPotions As List(Of MysteryPotion)
    Public invNeedsUDate As Boolean = False
    Public invIDorder As List(Of Integer)
    Dim sum As Integer = 0

    '|CONSTUCTOR|
    Sub New(Optional ByVal shufflePotions As Boolean = False)
        '0.1 - 0.4
        internal_inventory.Add("Compass", New Compass())                        '0
        internal_inventory.Add("Stick_of_Gum", New StickOfGum())                '1
        internal_inventory.Add("Health_Potion", New HealthPotion())             '2
        internal_inventory.Add("Vial_of_Slime", New VialOfSlime())              '3
        internal_inventory.Add("Spellbook", New Spellbook())                    '4
        internal_inventory.Add("Steel_Armor", New SteelArmor())                 '5
        internal_inventory.Add("Steel_Sword", New SteelSword())                 '6
        internal_inventory.Add("Steel_Bikini", New SteelBikini())               '7
        internal_inventory.Add("Chicken_Suit", New ChickenSuit())               '8
        internal_inventory.Add("SoulBlade", New SoulBlade())                    '9
        internal_inventory.Add("Magical_Girl_Outfit", New MagGirlOutfit())      '10
        internal_inventory.Add("Magical_Girl_Wand", New MagGirlWand())          '11
        internal_inventory.Add("Cat_Lingerie", New CatLingerie())               '12
        internal_inventory.Add("Mana_Potion", New ManaPotion())                 '13
        internal_inventory.Add("Restore_Potion", New RestorationPotion())       '14
        internal_inventory.Add("Cat_Ears", New CatEars())                       '15
        internal_inventory.Add("Bunny_Suit", New BunnySuit())                   '16
        internal_inventory.Add("Sorcerer's_Robes", New SorcerersRobes())        '17
        internal_inventory.Add("Witch_Cosplay", New WitchCosplay())             '18
        internal_inventory.Add("Warrior's_Cuirass", New WarriorsCuirass())      '19
        internal_inventory.Add("Brawler_Cosplay", New BrawlerCosplay())         '20
        internal_inventory.Add("Oak_Staff", New OakStaff())                     '21
        internal_inventory.Add("Wizard_Staff", New WizardStaff())               '22
        internal_inventory.Add("Bronze_Xiphos", New BronzeXiphos())             '23
        internal_inventory.Add("Sword_of_the_Brutal", New TargaxSword())        '24
        internal_inventory.Add("Breast_Growth_Potion", New BEPotion())          '25
        internal_inventory.Add("Hazardous_Potion", New HazardousPotion())       '26
        internal_inventory.Add("Gentle_Potion", New GentlePotion())             '27
        internal_inventory.Add("Sex_Swap_Potion", New GSPotion())               '28
        internal_inventory.Add("Feminine_Potion", New FemininePotion())         '29
        internal_inventory.Add("Chicken_Leg", New ChickenLeg())                 '30
        internal_inventory.Add("Apple​", New PApple())                           '31
        internal_inventory.Add("Apple", New Apple())                            '32
        internal_inventory.Add("Medicinal_Tea", New Herbs())                    '33
        internal_inventory.Add("Heavy_Cream", New HeavyCream())                 '34
        internal_inventory.Add("Cupcake", New Cupcake())                        '35
        internal_inventory.Add("Mirror", New Mirror())                          '36
        internal_inventory.Add("Glowstick", New Glowstick())                    '37
        internal_inventory.Add("Gold_Armor", New GoldArmor())                   '38
        internal_inventory.Add("Gold_Adornment", New GoldAdornment())           '39
        internal_inventory.Add("Gold_Sword", New GoldSword())                   '40
        internal_inventory.Add("Golden_Staff", New GoldenStaff())               '41
        internal_inventory.Add("Midas_Gauntlet", New MidasGuantlet())           '42
        internal_inventory.Add("Gold", New Gold())                              '43
        internal_inventory.Add("Angel_Food_Cake", New AngelFood())              '44
        internal_inventory.Add("Duster", New MaidDuster())                      '45
        internal_inventory.Add("Tank_Top", New TankTop())                       '46
        internal_inventory.Add("Sports_Bra", New SportBra())                    '47
        internal_inventory.Add("Health_Charm", New HealthCharm())               '48
        internal_inventory.Add("Mana_Charm", New ManaCharm())                   '49
        internal_inventory.Add("Attack_Charm", New AttackCharm())               '50
        internal_inventory.Add("Defense_Charm", New DefenseCharm())             '51
        internal_inventory.Add("Speed_Charm", New SpeedCharm())                 '52
        internal_inventory.Add("Key", New Key())                                '53
        internal_inventory.Add("Ropes", New Ropes())                            '54
        internal_inventory.Add("Living_Armor", New LiveArmor())                 '55
        internal_inventory.Add("Living_Lingerie", New LiveLingerie())           '56
        internal_inventory.Add("Disarment_Kit", New DisarmentKit())             '57
        internal_inventory.Add("Fusion_Crystal", New FusionCrystal())           '58
        internal_inventory.Add("Mysterious_Potion", New MysPotion())            '59
        internal_inventory.Add("Breast_Shrink._Potion", New BSPotion())         '60
        internal_inventory.Add("Masculine_Potion", New MasculinePotion())       '61
        internal_inventory.Add("Ditzy_Potion", New DitzyPotion())               '62
        internal_inventory.Add("Spidersilk_Whip", New SpidersilkWhip())         '63
        internal_inventory.Add("Chitin_Armor", New ChitArmor())                 '64
        '0.5
        internal_inventory.Add("Advanced_Spellbook", New ASpellbook())          '65
        '0.6
        internal_inventory.Add("Heart_Necklace", New HeartNecklace())           '66
        internal_inventory.Add("Red_Headband", New RedHeadband())               '67
        internal_inventory.Add("Ruby_Circlet", New RubyCirclet())               '68
        internal_inventory.Add("Slave_Collar", New ThrallCollar())              '69
        internal_inventory.Add("Cowbell", New Cowbell())                        '70
        internal_inventory.Add("Cow_Print_Bra", New CowBra())                   '71
        '0.7
        internal_inventory.Add("Maid_Outfit", New MaidOutfit())                 '72
        internal_inventory.Add("Goddess_Gown", New GoddessGown())               '73
        internal_inventory.Add("Succubus_Garb", New SuccubusGarb())             '74
        internal_inventory.Add("Regal_Gown", New PrincessGown())                '75
        internal_inventory.Add("Clear_Potion", New ClearPotion())               '76
        internal_inventory.Add("Minor_Ring_of_Regen.", New ROfMinRegen())       '77
        internal_inventory.Add("Val._Night_Lingerie", New VNightLingerie())     '78
        internal_inventory.Add("Val._Day_Suit", New VDayClothes())              '79
        internal_inventory.Add("Dissolved_Clothes", New DissolvedClothes())     '80     actually a part of 0.8
        internal_inventory.Add("Ring_of_Amaraphne", New ROAmaraphne())          '81
        internal_inventory.Add("Major_Health_Potion", New MajHealthPotion())    '82
        internal_inventory.Add("Bronze_Armor", New BronzeArmor())               '83
        internal_inventory.Add("Bronze_Battle_Axe", New BronzeAxe())            '84
        internal_inventory.Add("Bronze_Bikini", New BronzeBikini())             '85
        internal_inventory.Add("Portal_Chalk", New PortalChalk())               '86
        '0.8
        internal_inventory.Add("Bimbo_Lesson", New BimboLesson())               '87
        internal_inventory.Add("Combat_Manual", New CombatManual())             '88
        internal_inventory.Add("Utility_Manual", New UtilityManual())           '89
        internal_inventory.Add("Panacea", New Panacea())                        '90
        internal_inventory.Add("Vial_of_Venom", New VialOfVenom())              '91
        internal_inventory.Add("Anti_Venom", New AntiVenom())                   '92
        internal_inventory.Add("Blinding_Potion", New BlindPotion())            '93
        internal_inventory.Add("Armored_Bunny_Suit", New BunnySuitA())          '94
        internal_inventory.Add("Valkyrie_Armor", New ValkyrieArmor())           '95
        internal_inventory.Add("Valkyrie_Sword", New ValkyrieSword())           '96
        internal_inventory.Add("Bowtie", New Bowtie())                          '97
        internal_inventory.Add("Cursed_Heavy_Cream", New CHeavyCream())         '98
        internal_inventory.Add("Amazonian_Attire", New AmaAttire())             '99
        internal_inventory.Add("Cherry_Stick_of_Gum", New CStickOfGum())        '100
        internal_inventory.Add("Barbarian_Armor", New BarbArmor())              '101
        internal_inventory.Add("Space_Age_Jumpsuit", New SAJumpsuit())          '102
        internal_inventory.Add("Skin_Tight_Bodysuit", New STBodysuit())         '103
        internal_inventory.Add("Photon_Armor", New PhotonArmor())               '104
        internal_inventory.Add("Photon_Bikini", New PhotonBikini())             '105
        internal_inventory.Add("Labcoat", New Labcoat())                        '106
        internal_inventory.Add("Labcoat​", New LabcoatSV())                      '107
        internal_inventory.Add("Spatial_Shroom", New SShroom())                 '108
        internal_inventory.Add("Mint_Stick_of_Gum", New MStickOfGum())          '109
        internal_inventory.Add("Mobile_Powerbank", New Generator())             '110
        internal_inventory.Add("Discharge_Gauntlets", New ManaDisharge())       '111
        internal_inventory.Add("Photon_Blade", New PhotonBlade())               '112
        internal_inventory.Add("Amazon_Lesson", New AmazonLesson())             '113
        internal_inventory.Add("Basic_Class_Change", New BasicClassChange)      '114 (formerly Barbarian Lesson [edited v11.0]
        internal_inventory.Add("Warlock's_Robes", New WarlockRobe())            '115
        internal_inventory.Add("Gynoid_Uniform", New GCUniform())               '116
        internal_inventory.Add("Garden_Salad", New GardenSalad())               '117
        internal_inventory.Add("Corse_War_Axe", New CWarAxe())                  '118
        internal_inventory.Add("Broken_Remote", New BrokenRemote())             '119
        internal_inventory.Add("Shrink_Ray", New ShrinkRay())                   '120
        internal_inventory.Add("Name_Change", New NameChange())                 '121
        internal_inventory.Add("Gorgon_Lesson", New HGorgonLesson())            '122
        internal_inventory.Add("Ring_of_Uvona", New RingOfUvona())              '123
        internal_inventory.Add("Advanced_Class_Change", New AdvClassChange)     '124 (formerly Warlock Lesson [edited v11.0]
        internal_inventory.Add("Berry_Stick_of_Gum", New BBStickOfGum())        '125
        internal_inventory.Add("Omni_Charm", New OmniCharm())                   '126
        internal_inventory.Add("Vial_of_BIM_II", New VialOfBimbo())             '127
        internal_inventory.Add("CryoGrenade", New CryoGrenade())                '128
        internal_inventory.Add("Bunny_Suit​", New SVBunnySuit())                 '129
        internal_inventory.Add("Galaxy_Dye", New GalaxyDye())                   '130
        internal_inventory.Add("Base_Form_Reset", New BFormReset())             '131
        internal_inventory.Add("Melon_Stick_of_Gum", New WStickOfGum())         '132
        internal_inventory.Add("Warrior's_Feast", New WFeast())                 '133
        internal_inventory.Add("Mage's_Delicacy", New MDelicacy())              '134
        internal_inventory.Add("Tavern_Special", New TSpecial())                '135
        internal_inventory.Add("Vial_of_Pink_Slime", New VialOfPSlime())        '136
        internal_inventory.Add("Gelatinous_Shell", New GelArmor())              '137
        internal_inventory.Add("Gelatinous_Negligee", New GelLinge())           '138
        internal_inventory.Add("Braced_Headband", New BracedHeadband())         '139
        internal_inventory.Add("Emerald_Circlet", New EmeraldCirclet())         '140
        internal_inventory.Add("Active_Camoflage", New ActiveCamoflage())       '141
        internal_inventory.Add("Combat_Module", New CombatModule())             '142
        internal_inventory.Add("Every_New_Item", New EveryNewItem())            '143
        internal_inventory.Add("Crystalline_Armor", New CrystalArmor())         '144
        internal_inventory.Add("Scepter_of_Ash", New ScepterOfAsh())            '145
        '0.9                                                                    
        internal_inventory.Add("Cat_Armor", New CatArmor())                     '146
        internal_inventory.Add("Skimpy_Tank_Top", New SkimpyTT())               '147
        internal_inventory.Add("Grappling_Hook", New GrapplingHook())           '148
        internal_inventory.Add("Mana_Hibiscus", New ManaHibiscus())             '149
        internal_inventory.Add("Twin_Xiphoi", New TwinBlades())                 '150
        internal_inventory.Add("Lolita_Dress_(Sweet)", New SLolitaDress())      '151
        internal_inventory.Add("Will_Charm", New WillCharm())                   '152
        internal_inventory.Add("Anti_Curse_Tag", New AntiCurseTag())            '153
        internal_inventory.Add("New-U_Crystal", New NewUCrystal())              '154
        internal_inventory.Add("Bronze_Spear", New BronzeSpear())               '155
        internal_inventory.Add("Steel_Spear", New SteelSpear())                 '156
        internal_inventory.Add("Flaming_Spear", New FlamingSpear())             '157
        internal_inventory.Add("Signature_Spear", New SigSpear())               '158
        internal_inventory.Add("Signature_Staff", New SigStaff())               '159
        internal_inventory.Add("Spiked_Staff", New SpikedStaff())               '160
        internal_inventory.Add("Blindfold", New Blindfold())                    '161
        internal_inventory.Add("Throwing_Knife", New TKnife())                  '162
        internal_inventory.Add("Signature_Dagger", New SigDagger())             '163
        internal_inventory.Add("Stealth_Gear", New StealthGear())               '164
        internal_inventory.Add("Mugger's_Shank", New MShank())                  '165
        internal_inventory.Add("Frock_of_Night", New FoNight())                 '166
        internal_inventory.Add("Wand_of_Shocking", New WOShock())               '167
        internal_inventory.Add("Cursemark", New Cursemark())                    '168
        internal_inventory.Add("Maid_Lingerie", New MaidLingerie())             '169
        internal_inventory.Add("Magical_Slut_Outfit", New MagSlutOutfit())      '170
        internal_inventory.Add("​Magical_Girl_Wand​", New MagSlutWand())          '171
        internal_inventory.Add("Flaming_Sword", New FlamingSword())             '172
        internal_inventory.Add("Signature_Whip", New SigWhip())                 '173
        internal_inventory.Add("Defense_Charm​", New CdefenseCharm())            '174
        internal_inventory.Add("Cozy_Sweater", New CozySweater())               '175
        internal_inventory.Add("Scale_Armor", New ScaleArmor())                 '176
        internal_inventory.Add("Scale_Bikini", New ScaleBikini())               '177
        internal_inventory.Add("Antifreeze", New AntiFreeze())                  '178
        internal_inventory.Add("Wand_of_Voltage", New WOVoltage())              '179
        internal_inventory.Add("Pink_Panties", New PPanties())                  '180
        internal_inventory.Add("TODO_Outfit", New GothOutfit())                 '181
        internal_inventory.Add("Cursed_Coupon", New CursedCoupon())             '182
        internal_inventory.Add("Kitsune's_Robes", New KitsuneRobe())            '183
        'v0.9.1
        internal_inventory.Add("Common_Clothes", New CommonClothes0())          '184
        internal_inventory.Add("Common_Armor", New CommonClothes1())            '185
        internal_inventory.Add("Common_Garb", New CommonClothes2())             '186
        internal_inventory.Add("Fancy_Clothes", New CommonClothes3())           '187
        internal_inventory.Add("Ordinary_Clothes", New CommonClothes4())        '188
        internal_inventory.Add("Common_Kimono", New CommonClothes5())           '189
        internal_inventory.Add("Sneaky_Clothes", New CommonClothes6())          '190
        internal_inventory.Add("Skimpy_Clothes", New SkimpyClothes())           '191
        internal_inventory.Add("Very_Skimpy_Clothes", New VSkimpyClothes())     '192
        internal_inventory.Add("Ass_Growth_Potion", New UEPotion())             '193
        internal_inventory.Add("Dick_Growth_Potion", New DEPotion())            '194
        internal_inventory.Add("Curse_'B'_Gone", New CurseBGone())              '195
        internal_inventory.Add("Cow_Cosplay", New CowCosplay())                 '196
        internal_inventory.Add("Bimbell", New Bimbell())                        '197
        internal_inventory.Add("Kitsune_Mask", New KitsuneMask())               '198
        internal_inventory.Add("Angelic_Sweater", New AngelicSweater())         '199
        internal_inventory.Add("Attack_Charm​", New CAttackCharm())              '200
        internal_inventory.Add("Pro_Mag._Girl_Outfit", New ProMagGirlOutfit())  '201
        internal_inventory.Add("Mag._Girl_Outfit_(P)", New MagGirlOutfitP())    '202
        internal_inventory.Add("Pro_Mag._Girl_Wand", New ProMagGirlWand())      '203
        internal_inventory.Add("Mag._Girl_Wand_(P)", New MagGirlWandP())        '204
        internal_inventory.Add("Vial_of_Fire", New VialOfFire())                '205
        internal_inventory.Add("Gem_of_Progress", New GemOfProg())              '206
        internal_inventory.Add("Gem_of_Sweetness", New GemOfPink())             '207
        internal_inventory.Add("Mag._Girl_Outfit_(D)", New MagGirlOutfitD())    '208
        internal_inventory.Add("Mag._Girl_Wand_(D)", New MagGirlWandD)          '209
        internal_inventory.Add("Mag._Girl_Outfit_(R)", New MagGirlOutfitR)      '210
        internal_inventory.Add("Pro_Mag._G._Outfit_(R)", New ProMagGirlOutfitR) '211
        internal_inventory.Add("Mag._Girl_Wand_(R)", New MagGirlWandR)          '212
        internal_inventory.Add("Pro_Mag._G._Wand_(R)", New ProMagGirlWandR)     '213
        internal_inventory.Add("Gem_of_Flame", New GemOfFlame())                '214
        internal_inventory.Add("Gem_of_Darkness", New GemOfDark())              '215
        internal_inventory.Add("Mag._Girl_Outfit_(C)", New MagGirlOutfitC)      '216
        internal_inventory.Add("Demonic_Whip", New DemWhip)                     '217
        internal_inventory.Add("Archdemon_Whip", New ArchDemWhip)               '218
        internal_inventory.Add("Fox_Ears", New FoxEars)                         '219
        internal_inventory.Add("Skimpy_Clothes_(D)", New SkimpyClothesD)        '220
        internal_inventory.Add("Cow_Cosplay_(Demonic)", New CowCosplayD)        '221
        internal_inventory.Add("Bunny_Suit_(Classic)", New BunnySuitC)          '222
        internal_inventory.Add("Seven_Banded_Ring", New SevenBandedRing)        '223
        internal_inventory.Add("Fox_Statue", New FoxStatue)                     '224
        internal_inventory.Add("Bunny_Ears", New BunnyEars)                     '225
        internal_inventory.Add("Crimson_Spellbook", New CSpellbook)             '226
        internal_inventory.Add("Crimson_Manual", New CrimsonManual)             '227
        internal_inventory.Add("XP_Sandwich", New XPSandwich)                   '228
        internal_inventory.Add("BitGold", New BitGold)                          '229
        'v10.0.1
        internal_inventory.Add("Dragonfruit", New DragonFruit)                  '230
        internal_inventory.Add("Mental_Potion", New MentalPotion)               '231
        internal_inventory.Add("Chilling_Potion", New ChillingPotion)           '232
        internal_inventory.Add("Ass_Shrink._Potion", New USPotion)              '233
        internal_inventory.Add("Dick_Shrink._Potion", New DSPotion)             '234
        internal_inventory.Add("Hyper_Health_Potion", New HHealthPotion)        '235
        internal_inventory.Add("Hyper_Mana_Potion", New HManaPotion)            '236
        internal_inventory.Add("Succubus_Armor", New SuccubusArmor)             '237
        internal_inventory.Add("Lance_of_Darkness", New LanceOfDarkness)        '238
        internal_inventory.Add("Spidersilk_Bonds", New SpidersilkBonds)         '239
        internal_inventory.Add("Spidersilk_Bikini", New SpidersilkBikini)       '240
        internal_inventory.Add("Major_Mana_Potion", New MajManaPotion)          '241
        internal_inventory.Add("Spellcyclopedia", New Spellcyclopedia)          '242
        internal_inventory.Add("Big_Book_O'_Specials", New BookOSpecials)       '243
        internal_inventory.Add("Vial_of_Potent_Venom", New VialOfPotVenom)      '244
        internal_inventory.Add("Blight_Dismissal", New CursePurge)              '245
        internal_inventory.Add("Potion_of_Benediction", New BenedictPotion)     '246
        internal_inventory.Add("Potion_of_Dodging", New DodgePotion)            '247
        internal_inventory.Add("Incandescent_Potion", New IncandPotion)         '248
        internal_inventory.Add("Learn_'Focus_Up'", New TeachFocusedMantra)      '249
        internal_inventory.Add("Familiar's_Costume", New FamCostume)            '250
        'v11.0.0
        internal_inventory.Add("Old_Snips", New OldSnips)                       '251
        internal_inventory.Add("Collar_Snips", New CollarSnips)                 '252
        internal_inventory.Add("Cynn's_Mark", New CynnsMark)                    '253
        internal_inventory.Add("Adventurer's_Clothes", New CommonClothes7)      '254
        internal_inventory.Add("Platinum_Axe", New PlatinumAxe)                 '255
        internal_inventory.Add("Platinum_Daggers", New PlatinumDaggers)         '256
        internal_inventory.Add("Platinum_Staff", New PlatinumStaff)             '257
        internal_inventory.Add("Accursed_Blade", New CursedSword)               '258
        internal_inventory.Add("Bewitched_Wand", New BewitchedWand)             '259
        internal_inventory.Add("Jinxed_Whip", New JinxedWhip)                   '260
        internal_inventory.Add("AAAAAA_Battery", New A6Battery)                 '261
        internal_inventory.Add("Cow_Print_Armor", New CowArmor)                 '262
        internal_inventory.Add("Upgrade_Armor", New UpgradeArmor)               '263
        internal_inventory.Add("Armor_Fragments", New ArmorFragments)           '264
        internal_inventory.Add("Platinum_Armor", New PlatArmor)                 '265
        internal_inventory.Add("Platinum_Adornment", New PlatAdornment)         '266
        internal_inventory.Add("Roc_Drumstick", New RocDrumstick)               '267
        internal_inventory.Add("Dragonfruit_S._of_Gum", New DFStickOfGum)       '268
        internal_inventory.Add("Nature's_Kiss", New NatureKiss)                 '269
        internal_inventory.Add("Better_Medicinal_Tea", New BetterHerbs)         '270
        internal_inventory.Add("Mark_of_the_Ox", New MOTOx)                     '271
        internal_inventory.Add("""Normal""_Steak", New Steak)                   '272
        internal_inventory.Add("Phase_Pistol", New PhasePistol)                 '273
        internal_inventory.Add("Phase_Rifle", New PhaseRifle)                   '274
        internal_inventory.Add("Phase_Hammer", New PhaseHammer)                 '275
        internal_inventory.Add("Phase_Drill", New PhaseDrill)                   '276
        internal_inventory.Add("Paleomancer's_Diary", New PaleoDiary)           '277
        internal_inventory.Add("Marissa's_Notes", New MarissasNotes)            '278
        internal_inventory.Add("AAAAAA_Specification", New AAAAAASpecs)         '279
        internal_inventory.Add("Phase_Vibrator", New PhaseVibrator)             '280
        internal_inventory.Add("Phase_Deflector", New PhaseDeflector)           '281
        internal_inventory.Add("Time_Cop_Clothes", New TimeCopClothes)          '282
        internal_inventory.Add("Hallowed_Talisman", New HallowedTalisman)       '283
        internal_inventory.Add("Large_Stick", New LargeStick)                   '284
        internal_inventory.Add("Regular_Clothes", New CommonClothes8)           '285
        internal_inventory.Add("Tome_Of_Knowledge", New TomeOfKnowlege)         '286
        internal_inventory.Add("Extra_Life", New ExtraLife)                     '287
        internal_inventory.Add("Cultist's_Cloak", New CultistCloak)             '288
        internal_inventory.Add("Crimson_Cloak", New CrimsonCloak)               '289
        internal_inventory.Add("Skimpy_Clothes_(G)", New SkimpyClothesG)        '290
        internal_inventory.Add("Golden_Gum", New GoldenGum)                     '291
        internal_inventory.Add("Imitation_Cowbell", New ImmitationCowbell)      '292
        internal_inventory.Add("Magical_Mimic_Wand​", New MagMimicWand)          '293
        internal_inventory.Add("Vial_of_Rock_Juice", New VialOfRockJuice)       '294
        internal_inventory.Add("Ice_Pop", New IcePop)                           '295
        internal_inventory.Add("Ice_Pop​", New IcePopB)                          '296
        internal_inventory.Add("Staff_of_the_Tidemage", New TidemageStaff)      '297
        internal_inventory.Add("Lime_Bikini", New LimeBikini)                   '298
        internal_inventory.Add("Summertime_Shades", New SummerShades)           '299
        'v11.5.0
        internal_inventory.Add("Necromancer's_Robes", New NecromancerRobe)      '300
        internal_inventory.Add("Paladin's_Armor", New PaladinArmor)             '301
        internal_inventory.Add("Amazonian_Armor", New AmaArmor)                 '302
        internal_inventory.Add("Plant_Bikini", New PlantBikini)                 '303
        internal_inventory.Add("Mag._Girl_Outfit_(G)", New MagGirlOutfitG)      '304
        internal_inventory.Add("Pro_Mag._G._Outfit_(G)", New ProMagGirlOutfitG) '305
        internal_inventory.Add("Gem_of_Ivy", New GemOfIvy)                      '306
        internal_inventory.Add("Mag._Girl_Wand_(G)", New MagGirlWandG)          '307
        internal_inventory.Add("Red_Framed_Spectacles", New RedRimmedSpecs)     '308
        internal_inventory.Add("Small_Glasses", New SmallGlasses)               '309
        internal_inventory.Add("Circular_Glasses", New CircularGlasses)         '310
        internal_inventory.Add("Thick_Rimmed_Specs", New ThickRimmedSpecs)      '311
        internal_inventory.Add("Cool_Shades", New Shades)                       '312
        internal_inventory.Add("Monocle", New Monocle)                          '313
        internal_inventory.Add("Eyepatch", New Eyepatch)                        '314
        internal_inventory.Add("Masquerader's_Mask", New MasqueraderMask)       '315
        internal_inventory.Add("Cyber_Visor_(P)", New CyberVisorP)              '316
        internal_inventory.Add("Cyber_Visor_(O)", New CyberVisorO)              '317
        internal_inventory.Add("Cyber_Visor_(G)", New CyberVisorG)              '318
        internal_inventory.Add("All-Seeing_Shades", New AllSeeingShades)        '319
        internal_inventory.Add("Ball_Gag", New Ballgag)                         '320
        internal_inventory.Add("Spectral_Gag", New SpectralGag)                 '321

        armor = {New Naked,
                 Me.item(5), Me.item(7), Me.item(8), Me.item(10),
                 Me.item(12), Me.item(16), Me.item(17), Me.item(18),
                 Me.item(19), Me.item(20), Me.item(38), Me.item(39),
                 Me.item(46), Me.item(47), Me.item(54), Me.item(55),
                 Me.item(56), Me.item(64), Me.item(71), Me.item(72),
                 Me.item(73), Me.item(74), Me.item(75), Me.item(78),
                 Me.item(79), Me.item(83), Me.item(85), Me.item(80),
                 Me.item(94), Me.item(95), Me.item(99), Me.item(101),
                 Me.item(102), Me.item(103), Me.item(104), Me.item(105),
                 Me.item(106), Me.item(107), Me.item(115), Me.item(116),
                 Me.item(129), Me.item(137), Me.item(138), Me.item(144),
                 Me.item(146), Me.item(147), Me.item(151), Me.item(166),
                 Me.item(169), Me.item(170), Me.item(175), Me.item(176),
                 Me.item(177), Me.item(181), Me.item(183), Me.item(184),
                 Me.item(185), Me.item(186), Me.item(187), Me.item(188),
                 Me.item(189), Me.item(190), Me.item(191), Me.item(192),
                 Me.item(196), Me.item(199), Me.item(201), Me.item(202),
                 Me.item(208), Me.item(210), Me.item(211), Me.item(216),
                 Me.item(220), Me.item(221), Me.item(222), Me.item(237),
                 Me.item(239), Me.item(240), Me.item(250), Me.item(254),
                 Me.item(262), Me.item(265), Me.item(266), Me.item(282),
                 Me.item(285), Me.item(288), Me.item(289), Me.item(290),
                 Me.item(298), Me.item(300), Me.item(301), Me.item(302),
                 Me.item(303), Me.item(304), Me.item(305)}

        weapons = {New BareFists(),
                   Me.item(6), Me.item(9), Me.item(11), Me.item(21),
                   Me.item(22), Me.item(23), Me.item(24), Me.item(40),
                   Me.item(41), Me.item(42), Me.item(45), Me.item(63),
                   Me.item(84), Me.item(96), Me.item(111), Me.item(112),
                   Me.item(118), Me.item(120), Me.item(145), Me.item(150),
                   Me.item(155), Me.item(156), Me.item(157), Me.item(158),
                   Me.item(159), Me.item(160), Me.item(162), Me.item(163),
                   Me.item(165), Me.item(167), Me.item(171), Me.item(172),
                   Me.item(173), Me.item(179), Me.item(203), Me.item(204),
                   Me.item(209), Me.item(212), Me.item(213), Me.item(217),
                   Me.item(218), Me.item(238), Me.item(255), Me.item(256),
                   Me.item(257), Me.item(258), Me.item(259), Me.item(260),
                   Me.item(273), Me.item(274), Me.item(275), Me.item(276),
                   Me.item(284), Me.item(293), Me.item(297), Me.item(307)}

        useable = {Me.item(0), Me.item(3), Me.item(4), Me.item(65),
                   Me.item(15), Me.item(36), Me.item(37), Me.item(45),
                   Me.item(48), Me.item(49), Me.item(50), Me.item(51),
                   Me.item(52), Me.item(57), Me.item(58), Me.item(81),
                   Me.item(86), Me.item(88), Me.item(89), Me.item(91),
                   Me.item(119), Me.item(126), Me.item(127), Me.item(128),
                   Me.item(130), Me.item(136), Me.item(142), Me.item(143),
                   Me.item(148), Me.item(149), Me.item(152), Me.item(153),
                   Me.item(154), Me.item(155), Me.item(156), Me.item(157),
                   Me.item(158), Me.item(162), Me.item(174), Me.item(182),
                   Me.item(195), Me.item(200), Me.item(205), Me.item(206),
                   Me.item(207), Me.item(214), Me.item(215), Me.item(219),
                   Me.item(226), Me.item(227), Me.item(238), Me.item(244),
                   Me.item(251), Me.item(252), Me.item(277), Me.item(278),
                   Me.item(279), Me.item(280), Me.item(286), Me.item(294),
                   Me.item(306)}

        food = {Me.item(1), Me.item(30), Me.item(31), Me.item(32),
                Me.item(33), Me.item(34), Me.item(35), Me.item(44),
                Me.item(90), Me.item(98), Me.item(100), Me.item(108),
                Me.item(109), Me.item(117), Me.item(125), Me.item(132),
                Me.item(133), Me.item(134), Me.item(135), Me.item(178),
                Me.item(228), Me.item(230), Me.item(267), Me.item(268),
                Me.item(269), Me.item(270), Me.item(272), Me.item(291),
                Me.item(295), Me.item(296)}

        acce = {New noAcce(),
                Me.item(66), Me.item(67), Me.item(68), Me.item(69),
                Me.item(70), Me.item(77), Me.item(81), Me.item(97),
                Me.item(110), Me.item(123), Me.item(139), Me.item(140),
                Me.item(141), Me.item(149), Me.item(164), Me.item(168),
                Me.item(180), Me.item(197), Me.item(198), Me.item(223),
                Me.item(225), Me.item(253), Me.item(271), Me.item(281),
                Me.item(283), Me.item(292), Me.item(320), Me.item(321)}

        potions = {Me.item(2), Me.item(13), Me.item(14), Me.item(25),
                   Me.item(26), Me.item(27), Me.item(28), Me.item(29),
                   Me.item(59), Me.item(60), Me.item(61), Me.item(62),
                   Me.item(76), Me.item(82), Me.item(92), Me.item(93),
                   Me.item(193), Me.item(194), Me.item(231), Me.item(232),
                   Me.item(233), Me.item(234), Me.item(235), Me.item(236),
                   Me.item(241), Me.item(246), Me.item(247), Me.item(248)}

        Array.Sort(potions)

        misc = {Me.item(43), Me.item(53), Me.item(224), Me.item(229),
                Me.item(242), Me.item(243), Me.item(261), Me.item(264),
                Me.item(287)}

        glasses = {New noGlasses(),
                   Me.item(161), Me.item(299), Me.item(308), Me.item(309),
                   Me.item(310), Me.item(311), Me.item(312), Me.item(313),
                   Me.item(314), Me.item(315), Me.item(316), Me.item(317),
                   Me.item(318), Me.item(319)}

        invIDorder = New List(Of Integer)

        If shufflePotions Then setPotionNames()
    End Sub
    Sub setPotionNames()
        Dim names = New List(Of String)
        names.AddRange({"Red_Potion", "Green_Potion", "Blue_Potion", "Golden_Potion",
                      "Azure_Potion", "Rose_Potion", "Mauve_Potion", "Jet_Potion",
                      "Ruby_Potion", "Emerald_Potion", "Sapphire_Potion", "Silver_Potion",
                      "Murky_Potion", "Smokey_Potion", "Pink_Potion", "Glittery_Potion",
                      "Florecent_Potion", "Coral_Potion", "Steely_Potion", "Cyan_Potion",
                      "Violet_Potion", "Pewter_Potion", "Pearly_Potion", "Verdant_Potion",
                      "Lilac_Potion"})
        mPotions = New List(Of MysteryPotion)
        For i = 0 To UBound(potions)
            If potions(i).GetType().IsSubclassOf(GetType(MysteryPotion)) Then mPotions.Add(potions(i))
        Next
        Randomize()
        For Each p In mPotions
            Dim r = Int(Rnd() * names.Count)
            p.setFName(names(r))
            names.RemoveAt(r)
        Next
    End Sub

    '|UTILITY|
    Sub merge(ByRef inv As Inventory)
        For i = 0 To upperBound()
            item(i).add(inv.item(i).count)
        Next
    End Sub
    Sub mergeRevalue(ByRef inv As Inventory)
        For i = 0 To upperBound()
            item(i).value = inv.item(i).value
            item(i).add(inv.item(i).count)
        Next
    End Sub
    Sub add(ByVal key As String, ByVal count As Integer)
        If internal_inventory.Keys.Contains(key) Then
            internal_inventory(key).add(count)
        Else
            DDError.missingInvItemError(key)
        End If
        sum += count
    End Sub
    Sub add(ByVal id As Integer, ByVal count As Integer)
        If id >= 0 And id <= upperBound() Then
            Dim key = internal_inventory.Keys(id)
            internal_inventory(key).add(count)
        End If
        sum += count
    End Sub
    Sub setCount(k As String, v As Integer)
        sum -= item(k).count
        If internal_inventory.Keys.Contains(k) Then
            item(k).count = v
        End If
        sum += v
    End Sub
    Sub setCount(i As Integer, v As Integer)
        sum -= item(i).count
        item(i).count = v
        sum += v
    End Sub

    '|SAVE/LOAD|
    Function save() As String
        Dim out = CStr(upperBound()) & ":"
        For i = 0 To upperBound()
            out += getKeyByID(i) & "~" & item(i).count & "~" & item(i).durability & ":"
        Next

        If Not mPotions Is Nothing Then
            out += CStr(mPotions.Count - 1) & ":"
            For i = 0 To mPotions.Count - 1
                out += mPotions(i).getName() & "~" & mPotions(i).hasBeenUsed & ":"
            Next
        Else
            out += "na:"
        End If

        out += "*"
        Return out
    End Function
    Sub load(ByVal input As String)
        Dim parse As String() = input.Split(":")

        For i As Integer = 1 To parse(0) + 1
            Dim subParse As String() = parse(i).Split("~")
            If internal_inventory.Keys.Contains(subParse(0)) Then
                add(subParse(0), CInt(subParse(1)))
                item(i - 1).durability = CInt(subParse(2))
            End If
        Next
        Dim x = CInt(parse(0)) + 2

        If Not parse(x).Equals("na") And Not mPotions Is Nothing Then
            For i = x + 1 To x + CInt(parse(x))
                Dim subParse As String() = parse(i).Split("~")
                mPotions(i - (x + 1)).setFName(subParse(0))
                mPotions(i - (x + 1)).hasBeenUsed = CBool(subParse(1))
                If mPotions(i - (x + 1)).hasBeenUsed Then mPotions(i - (x + 1)).reveal()
            Next
        End If
        calcSum()
    End Sub

    '|GETTERS|
    Public Function item(ByVal n As String) As Item
        If internal_inventory.Keys.Contains(n) Then
            Return internal_inventory(n)
        Else
            For Each p In potions
                If p.getName = n Then Return p
            Next
            Return Nothing
        End If
    End Function
    Public Function item(ByVal id As Integer) As Item
        If id < 0 Or id > upperBound() Then Return Nothing
        Dim key = internal_inventory.Keys(id)
        Return internal_inventory(key)
    End Function
    Public Function getKeyByID(ByVal id As Integer) As String
        Return internal_inventory.Keys(id)
    End Function
    Public Function idOfKey(ByVal n As String) As Integer
        If Not internal_inventory.Keys.Contains(n) Then Return -1
        Return item(n).id
    End Function
    Public Function getCountAt(ByVal i As Integer) As Integer
        If item(i) Is Nothing Then Return 0
        Return item(i).getCount()
    End Function
    Public Function getCountAt(ByVal n As String) As Integer
        If item(n) Is Nothing Then Return 0
        Return item(n).getCount()
    End Function
    Public Function calcSum() As Integer
        Dim totalSum As Integer = 0
        For i = 0 To upperBound()
            totalSum += getCountAt(i)
        Next

        sum = totalSum

        Return totalSum
    End Function

    Function getArmors() As Tuple(Of String(), Armor())
        Dim s(UBound(armor)) As String
        For i = 0 To UBound(armor)
                s(i) = armor(i).getName
        Next
        Return New Tuple(Of String(), Armor())(s, armor)
    End Function
    Function getWeapons() As Tuple(Of String(), Weapon())
        Dim s(UBound(weapons)) As String
        For i = 0 To UBound(weapons)
            s(i) = weapons(i).getName
        Next
        Return New Tuple(Of String(), Weapon())(s, weapons)
    End Function
    Function getAccesories() As Tuple(Of String(), Accessory())
        Dim s(UBound(acce)) As String
        For i = 0 To UBound(acce)
            s(i) = acce(i).getName
        Next
        Return New Tuple(Of String(), Accessory())(s, acce)
    End Function
    Function getUseable() As Item()
        Return useable
    End Function
    Function getFood() As Item()
        Return food
    End Function
    Function getPotions() As Item()
        Return potions
    End Function
    Function getMisc() As Item()
        Return misc
    End Function
    Function getGlasses() As Tuple(Of String(), Glasses())
        Dim s(UBound(glasses)) As String
        For i = 0 To UBound(glasses)
            s(i) = glasses(i).getName
        Next
        Return New Tuple(Of String(), Glasses())(s, glasses)

    End Function
    Function getMPotions() As List(Of MysteryPotion)
        Return mPotions
    End Function
    Public Function getSum() As Integer
        Return sum
    End Function

    Function upperBound() As Integer
        Return internal_inventory.Count - 1
    End Function
    Function count() As Integer
        Return internal_inventory.Count
    End Function
End Class
