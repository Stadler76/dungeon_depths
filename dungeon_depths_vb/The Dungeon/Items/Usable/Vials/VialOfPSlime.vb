﻿Public Class VialOfPSlime
    Inherits Item
    Sub New()
        '|ID Info|
        setName("Vial_of_Pink_Slime")
        id = 136
        tier = 2

        '|Item Flags|
        usable = true
        droppable = True

        '|Stats|
        count = 0
        value = 400

        '|Description|
        setDesc("A glass bottle filled with an rose-colored non-newtonian gel.")
    End Sub

    Overrides Sub use(ByRef p As Player)
        If Me.getUsable() = False Then Exit Sub

        TextEvent.pushLog("You apply the " & getName())

        If p.formName.Equals("Slime") Or p.formName.Equals("Goo Girl") Then
            p.health = Math.Min(1, p.health + 0.45)

            count -= 1
            Exit Sub
        End If
        If p.perks(perk.googirltf) = -1 Or p.prt.haircolor.A = 255 Then
            p.perks(perk.googirltf) = 2
        End If

        p.ongoingTFs.Add(New GooGirlTF(p.perks(perk.googirltf)))
        p.update()
        count -= 1

    End Sub
End Class
