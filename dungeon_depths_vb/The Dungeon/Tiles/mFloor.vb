﻿Imports System.ComponentModel
Public Class mFloor
    Public mBoardWidth As Integer = 60
    Public mBoardHeight As Integer = 60
    Dim coveredBoardSpace As Integer = 0

    Public mBoard(,) As mTile
    Dim rooms As List(Of List(Of Room)) = New List(Of List(Of Room))
    Public floorNumber As Integer = 0
    Public floorCode As String
    Public stairs As Point

    Public chestList As List(Of Chest) = New List(Of Chest)
    Public statueList As List(Of Statue) = New List(Of Statue)
    Public trapList As List(Of Trap) = New List(Of Trap)

    Public playerPosition As Point = New Point(-1, -1)
    Public npcPositions As List(Of Point) = New List(Of Point)

    Public beatBoss As Boolean = False

    Public sessions As Dictionary(Of Integer, Session) = New Dictionary(Of Integer, Session)

    Public Shared nonRandomFloors() As Integer = {9999, 10000, 91017, 5, 75, 9}

    Public Sub New(ByVal code As String, ByVal fNum As Integer,
                   Optional ByVal bh As Integer = -1,
                   Optional ByVal bw As Integer = -1)
        Dim updateLoadbar = False
        updateLoadbar = Not Game.picLoadBar.Visible

        If updateLoadbar Then Game.initLoadBar()
        floorNumber = fNum
        floorCode = code
        If bh = -1 Then mBoardHeight = Game.mBoardHeight Else mBoardHeight = bh
        If bw = -1 Then mBoardWidth = Game.mBoardWidth Else mBoardWidth = bw

        defineBoardSpace()

        If nonRandomFloors.Contains(floorNumber) Then
            Game.updateLoadbar(99)
            Game.boardWorker.CancelAsync()
            Exit Sub
        End If
        If updateLoadbar Then Game.updateLoadbar(40)

        placeStairs()
        placePlayer(Game.player1)

        If floorNumber > 5 And Not nonRandomFloors.Contains(floorNumber) Then verifyNoDisconectedChunks(Game.player1)

        placeChest(floorCode)
        If floorNumber > 2 Then placeTraps()

        placeNPCs(Game.shop_npc_list, getPossibleNPCs)
        If updateLoadbar Then Game.updateLoadbar(70)

        If updateLoadbar Then
            Game.updateLoadbar(99)
            Game.boardWorker.CancelAsync()
        End If

        'If Not sessions.ContainsKey(Game.sessionID) Then
        '    sessions.Add(Game.sessionID, New Session(Game.sessionID, Game.player1.pos, beatBoss))
        'End If
    End Sub
    Public Sub New(ByVal code As String, Optional readFromFile As Boolean = True)
        If Not readFromFile Then
            loadMFloor(code)
        Else
            readFloorFromFile(code)
        End If
    End Sub

    '|---GENERAL FLOOR GENERATION METHODS---|
    Sub defineBoardSpace()
        'reset the board
        ReDim mBoard(mBoardHeight, mBoardWidth)
        For y = 0 To mBoardHeight
            For x = 0 To mBoardWidth
                mBoard(y, x) = New mTile(0, "", Color.Black)
            Next
        Next
        For Each n In Game.shop_npc_list
            n.pos = New Point(-1, -1)
        Next


        If floorNumber < 5 Then
            generateDungeonLevel(floorCode)
        ElseIf floorNumber = 5 Or floorNumber = 75 Then
            genBossFloor(Game.player1)
            'ElseIf floorNumber = 8 Then
            '    genFloor8()
        ElseIf floorNumber = 9 Then
            genFloor9()
        ElseIf floorNumber = 9999 Then
            genSpaceFloor()
        ElseIf floorNumber = 10000 Then
            genSpaceFloor2()
        ElseIf floorNumber = 91017 Then
            genLegacyFloor()
        ElseIf floorNumber > 5 Then
            generateForestLevel(floorCode)
        End If

        If floorNumber = 7 Then placeFloor7Statues()
    End Sub

    '|-Dungeon Floors-|
    Sub generateDungeonLevel(ByVal code As String)
        'generateLevel creates the random rooms and corridors of each level
        floorCode = code
        Rnd(-1)
        Randomize(code.GetHashCode)

        Dim maxBoardSpace As Integer = mBoardWidth * mBoardHeight
        Dim roomRow As List(Of Room) = New List(Of Room)
        Dim cursor As Point = New Point(0, 5)

        '|CREATE THE ROOMS|
        Dim prevProgress As Double = -1
        While (coveredBoardSpace / maxBoardSpace) < 0.75
            'define a new room:  Each iteration get smaller
            For i = 0 To 18
                'define the new room's length
                Dim l = Int(Rnd() * (16 - i)) + 2
                Dim h = Int(Rnd() * (16 - i)) + 2

                'define the new room's x and y positions
                Dim x = cursor.X
                'y +- 10% of the board's height 
                Dim y = cursor.Y - (Int(Rnd() * 10)) + ((Int(Rnd() * 20)))
                If x + l < mBoardWidth And x >= 0 And y + h < mBoardHeight And y >= 0 Then
                    Dim roomToPlace = New Room(New Point(x, y), l, h)
                    roomRow.Add(roomToPlace)
                    For j = 0 To h
                        For k = 0 To l
                            placeTile(k + x, j + y, False)
                        Next
                    Next


                    'move the cursor to the next room
                    Dim newCursorX = x + l + 1 + Int(Rnd() * 20)
                    Dim newCursorY = cursor.Y
                    If newCursorX >= mBoardWidth - 4 Then
                        rooms.Add(roomRow)
                        roomRow = New List(Of Room)
                        newCursorX = 0 + Int(Rnd() * 3)
                        newCursorY = cursor.Y + 20
                    End If


                    cursor = New Point(newCursorX, newCursorY)
                    Exit For
                End If
            Next
            If prevProgress = (coveredBoardSpace / maxBoardSpace) Then Exit While
            prevProgress = (coveredBoardSpace / maxBoardSpace)
        End While

        '|CONNECT THE ROOMS|
        Dim allrooms As List(Of Room) = New List(Of Room)()
        For j = 0 To rooms.Count - 1
            For i = 0 To rooms(j).Count - 1
                'get the room in question
                Dim r = rooms(j)(i)
                allrooms.Add(r)


                Dim potentialNeighbors As List(Of Room) = getPotentialNeigbors(rooms, i, j)
                If potentialNeighbors.Count = 0 Then Continue For

                'set the number of exits on the room
                Dim numExits = Int(Rnd() * potentialNeighbors.Count) + 1
                For num = 1 To numExits
                    connectRooms(r, potentialNeighbors(Int(Rnd() * potentialNeighbors.Count)))
                Next

                r.marked = True
            Next
        Next

        '|VERIFY NO DISCONECTED CHUNKS|
        For i = 0 To allrooms.Count - 1
            For j = i + 1 To allrooms.Count - 1
                If Not allrooms(i).connectedTo(allrooms(j)) Then connectRooms(allrooms(i), allrooms(j), True)
            Next
        Next
    End Sub
    Private Sub placeTile(ByVal x As Integer, ByVal y As Integer, ByVal seen As Boolean, Optional ByVal glow As Boolean = False)
        mBoard(y, x).Tag = If(seen, 2, 1)

        If glow Then
            mBoard(y, x).Text = "x"
        End If

        coveredBoardSpace += 1
    End Sub
    Private Sub connectRooms(ByRef r1 As Room, ByRef r2 As Room, Optional overrideFlag As Boolean = False)
        If (r1.connectedTo(r2) Or r2.connectedTo(r1)) And Not overrideFlag Then Exit Sub
        If r1.directConnections > Room.MAX_DIRECT_CONNECTIONS Or r2.directConnections > Room.MAX_DIRECT_CONNECTIONS Then Exit Sub

        r1.directConnections += 1
        r2.directConnections += 1

        connectPoints(r1.getExit, r2.getExit)

        r1.connect(r2)
        r2.connect(r1)
    End Sub
    Sub connectPoints(ByVal p1 As Point, ByVal p2 As Point)
        p1 = New Point(Math.Max(p1.X, 0), Math.Max(p1.Y, 0))
        p2 = New Point(Math.Max(p2.X, 0), Math.Max(p2.Y, 0))
        p1 = New Point(Math.Min(p1.X, mBoardWidth - 1), Math.Min(p1.Y, mBoardHeight - 1))
        p2 = New Point(Math.Min(p2.X, mBoardWidth - 1), Math.Min(p2.Y, mBoardHeight - 1))

        If p1.X.Equals(p2.X) And p1.Y.Equals(p2.Y) Then Exit Sub

        'Connects the entrances/exits of the rooms
        If p1.Y > p2.Y Then
            'p1 is below p2
            If p1.X > p2.X Then
                'p1 is right of p2
                goUpThenLeft(p1, p2)
            Else
                'p1 is left of p2
                goUpThenRight(p1, p2)
            End If
        Else
            'p1 is above p2
            If p1.X > p2.X Then
                'p1 is right of p2
                goDownThenLeft(p1, p2)
            Else
                'p1 is left of p2
                goDownThenRight(p1, p2)
            End If
        End If
    End Sub
    Private Sub goUpThenRight(ByVal p1 As Point, ByVal p2 As Point)
        'go up from p1's Y to p2's Y
        For y = p1.Y To p2.Y Step -1
            y = Math.Max(0, y)
            y = Math.Min(mBoardHeight, y)

            Dim x = Math.Max(0, p1.X)
            x = Math.Min(mBoardWidth, p1.X)

            If Not mBoard(y, x).Tag = 2 Then
                placeTile(x, y, False)
            End If
        Next

        'go right from p1's X to p2's X
        For x = p1.X To p2.X
            Dim y = Math.Max(0, p2.Y)
            y = Math.Min(mBoardHeight, p2.Y)

            x = Math.Max(0, x)
            x = Math.Min(mBoardWidth, x)

            If Not mBoard(y, x).Tag = 2 Then
                placeTile(x, y, False)
            End If
        Next
    End Sub
    Private Sub goUpThenLeft(ByVal p1 As Point, ByVal p2 As Point)
        'go up from p1's Y to p2's Y
        For y = p1.Y To p2.Y Step -1
            y = Math.Max(0, y)
            y = Math.Min(mBoardHeight, y)

            Dim x = Math.Max(0, p1.X)
            x = Math.Min(mBoardWidth, p1.X)

            If Not mBoard(y, x).Tag = 2 Then
                placeTile(x, y, False)
            End If
        Next

        'go left from p1's X to p2's X
        For x = p1.X To p2.X Step -1
            Dim y = Math.Max(0, p2.Y)
            y = Math.Min(mBoardHeight, y)

            x = Math.Max(0, x)
            x = Math.Min(mBoardWidth, x)

            If Not mBoard(y, x).Tag = 2 Then
                placeTile(x, y, False)
            End If
        Next
    End Sub
    Private Sub goDownThenRight(ByVal p1 As Point, ByVal p2 As Point)
        'go up from p1's Y to p2's Y
        For y = p1.Y To p2.Y
            y = Math.Max(0, y)
            y = Math.Min(mBoardHeight, y)

            Dim x = Math.Max(0, p1.X)
            x = Math.Min(mBoardWidth, p1.X)

            If Not mBoard(y, x).Tag = 2 Then
                placeTile(x, y, False)
            End If
        Next

        'go right from p1's X to p2's X
        For x = p1.X To p2.X
            Dim y = Math.Max(0, p2.Y)
            y = Math.Min(mBoardHeight, y)

            x = Math.Max(0, x)
            x = Math.Min(mBoardWidth, x)

            If Not mBoard(y, x).Tag = 2 Then
                placeTile(x, y, False)
            End If
        Next
    End Sub
    Private Sub goDownThenLeft(ByVal p1 As Point, ByVal p2 As Point)
        'go up from p1's Y to p2's Y
        For y = p1.Y To p2.Y
            y = Math.Max(0, y)
            y = Math.Min(mBoardHeight, y)

            Dim x = Math.Max(0, p1.X)
            x = Math.Min(mBoardWidth, p1.X)

            If Not mBoard(y, x).Tag = 2 Then
                placeTile(x, y, False)
            End If
        Next

        'go left from p1's X to p2's X
        For x = p1.X To p2.X Step -1
            Dim y = Math.Max(0, p2.Y)
            y = Math.Min(mBoardHeight, y)

            x = Math.Max(0, x)
            x = Math.Min(mBoardWidth, x)

            If Not mBoard(y, x).Tag = 2 Then
                placeTile(x, y, False)
            End If
        Next
    End Sub
    Private Shared Function getPotentialNeigbors(ByRef allRooms As List(Of List(Of Room)), ByVal x As Integer, ByVal y As Integer) As List(Of Room)
        Dim results As List(Of Room) = New List(Of Room)

        If x > 0 Then results.Add(allRooms(y)(x - 1))
        If x < allRooms(y).Count - 1 Then results.Add(allRooms(y)(x + 1))
        If y > 0 AndAlso x < allRooms(y - 1).Count - 1 Then results.Add(allRooms(y - 1)(x))
        If y < allRooms.Count - 1 AndAlso x < allRooms(y + 1).Count - 1 Then results.Add(allRooms(y + 1)(x))

        Console.WriteLine(results.Count)
        Return results
    End Function
    'floor 4
    Function ptInBounds(ByVal pt As Point) As Boolean
        Return pt.X >= 0 And pt.X <= mBoardWidth And pt.Y >= 0 And pt.Y <= mBoardHeight
    End Function
    Sub placeFloor4TrappedChest(ByRef p As Player)
        Dim c As Chest = New LoadedChest(getRndAdjPoint(p), 4)
        chestList.Add(c)
        mBoard(c.pos.Y, c.pos.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(c.pos.Y, c.pos.X).Text = "#"
    End Sub
    Public Function getRndAdjPoint(ByVal p As Player) As Point
        Dim possiblePoints = {New Point(p.pos.X + 1, p.pos.Y), _
                                  New Point(p.pos.X - 1, p.pos.Y), _
                                  New Point(p.pos.X, p.pos.Y + 1), _
                                  New Point(p.pos.X, p.pos.Y - 1), _
                                  New Point(p.pos.X + 1, p.pos.Y + 1), _
                                  New Point(p.pos.X - 1, p.pos.Y - 1), _
                                  New Point(p.pos.X + 1, p.pos.Y - 1), _
                                  New Point(p.pos.X - 1, p.pos.Y + 1)}
        Dim pt As Point = possiblePoints(0)
        Dim i = 0
        Do While (Not ptInBounds(pt) OrElse mBoard(pt.Y, pt.X).Tag < 1 Or mBoard(pt.Y, pt.X).Text <> "") And i < possiblePoints.Count - 1
            i += 1
            pt = possiblePoints(i)
        Loop

        Return pt
    End Function

    '|-Forest Floors-|
    Sub generateForestLevel(ByVal code As String)
        'generateLevel creates the random rooms and corridors of each level
        floorCode = code
        Rnd(-1)
        Randomize(code.GetHashCode)
        'Dump into area And agregate Map generator'
        Dim numRooms As Integer = CInt(Int((Rnd() * 5) + 25)) 'Create a random number of rooms
        Dim radius As Integer = CInt(20) ' Set the randius to the average of the board hight & width
        Dim RoomXY As List(Of Point) = New List(Of Point)
        Dim RoomWH As List(Of Point) = New List(Of Point)

        For i = 0 To numRooms
            Dim t = 2 * Math.PI * Rnd() 'a point around a circle
            Dim u = Rnd() + Rnd() 'a points radius
            Dim rad As Double = 0
            If u > 1 Then 'make sure that it isn't 0
                rad = 2 - u
            End If
            If u < 1 Then
                rad = u
            End If
            Dim pos As Point = New Point(Int(radius * rad * Math.Cos(t)), Int(radius * rad * Math.Sin(t))) 'Place the point multiplied by given radius
            RoomXY.Add(pos)
            'look into generating a poison distribution
            'Create a random width and lenth for each room
            Dim dime As Point = New Point(Int((Rnd() * 5) + 3), CInt((Rnd() * 5) + 3))
            RoomWH.Add(dime)
        Next

        'Run through all the rooms and check if they overlap
        For i = 0 To numRooms - 1
            Dim aPos As Point = RoomXY(i)
            Dim aDime As Point = RoomWH(i)
            For j = 0 To numRooms - 1
                Dim bPos As Point = RoomXY(j)
                Dim bDime As Point = RoomWH(j)
                If Not (aPos = bPos) And Not (aDime = bDime) Then


                    'Check for overlapping
                    Dim H_Overlaps As Boolean = (aPos.X <= bPos.X + bDime.X) AndAlso (bPos.X <= aPos.X + aDime.X)
                    Dim V_Overlaps As Boolean = (aPos.Y <= bPos.Y + bDime.Y) AndAlso (bPos.Y <= aPos.Y + aDime.Y)
                    If H_Overlaps AndAlso V_Overlaps Then
                        'Find the minimum amount of movment that stops the squares from touching
                        Dim dx = Math.Min(Math.Abs((aPos.X + aDime.X) - (bPos.X + 3)), Math.Abs(aPos.X - (bPos.X + bDime.X + 3)))
                        Dim dy = Math.Min(Math.Abs((aPos.Y + aDime.Y) - (bPos.Y + 3)), Math.Abs(aPos.Y - (bPos.Y + bDime.Y + 3)))
                        If dx <= dy Then
                            dy = 0
                        Else
                            dx = 0
                        End If
                        If aPos.X >= bPos.X Then
                            RoomXY(i) = New Point(RoomXY(i).X + Int(dx / 2), RoomXY(i).Y)
                            RoomXY(j) = New Point(RoomXY(j).X - Int(dx / 2), RoomXY(j).Y)
                        Else
                            RoomXY(i) = New Point(RoomXY(i).X - Int(dx / 2), RoomXY(i).Y)
                            RoomXY(j) = New Point(RoomXY(j).X + Int(dx / 2), RoomXY(j).Y)
                        End If
                        If aPos.Y >= bPos.Y Then
                            RoomXY(i) = New Point(RoomXY(i).X, RoomXY(i).Y + Int(dy / 2))
                            RoomXY(j) = New Point(RoomXY(j).X, RoomXY(j).Y - (dy / 2))
                        Else
                            RoomXY(i) = New Point(RoomXY(i).X, RoomXY(i).Y - Int(dy / 2))
                            RoomXY(j) = New Point(RoomXY(j).X, RoomXY(j).Y + Int(dy / 2))
                        End If


                    End If
                End If

            Next

        Next

        Dim exits As List(Of Point) = New List(Of Point)
        For i = 0 To numRooms - 1

            Dim RoomPos As Point = New Point(CInt(RoomXY(i).X + (mBoardWidth / 2)), CInt(Int(RoomXY(i).Y + (mBoardWidth / 2))))
            Dim RoomSpanY As Integer = RoomPos.Y + CInt(RoomWH(i).Y)
            Dim RoomSpanX As Integer = RoomPos.X + CInt(RoomWH(i).X)
            If RoomSpanY >= mBoardHeight - 1 Then RoomSpanY = mBoardHeight - 1
            If RoomSpanY < 0 Then RoomSpanY = 0
            If RoomSpanX >= mBoardWidth Then RoomSpanX = mBoardWidth - 1
            If RoomSpanX < 0 Then RoomSpanX = 0
            'Randomly place a special tag
            If Int(Rnd() * 3) = 0 Then
                For yP = RoomPos.Y To RoomSpanY
                    For xP = RoomPos.X To RoomSpanX
                        If xP < mBoardWidth And yP < mBoardHeight And xP > 0 And yP > 0 Then mBoard(yP, xP).Tag = 2 'Colour in the square
                    Next
                Next
                'else just colour it
            Else
                For yP = RoomPos.Y To RoomSpanY
                    For xp = RoomPos.X To RoomSpanX
                        If xp < mBoardWidth And yP < mBoardHeight And xp > 0 And yP > 0 Then mBoard(yP, xp).Tag = 1 'Colour in the square
                    Next
                Next
            End If

            Dim numExits As Integer = Int(Rnd() * 3)
            Dim mainExit As Point
            Select Case Int(Rnd() * 2)
                Case 0
                    mainExit = (New Point(RoomPos.X + 2, Int(Rnd() * (RoomSpanY - RoomPos.Y)) + RoomPos.Y))
                    If mainExit.X - 1 < mBoardWidth And mainExit.X - 1 > 0 And mainExit.Y - 1 < mBoardHeight And mainExit.Y - 1 > 0 AndAlso Not mBoard(mainExit.Y, mainExit.X - 1).Tag = 2 Then mBoard(mainExit.Y, mainExit.X - 1).Tag = 1
                Case Else
                    mainExit = (New Point(Int(Rnd() * (RoomSpanX - RoomPos.X)) + RoomPos.X, RoomPos.Y + 2))
                    If mainExit.X - 1 < mBoardWidth And mainExit.X - 1 > 0 And mainExit.Y - 1 < mBoardHeight And mainExit.Y - 1 > 0 AndAlso Not mBoard(mainExit.Y - 1, mainExit.X).Tag = 2 Then mBoard(mainExit.Y - 1, mainExit.X).Tag = 1
            End Select
            If i > 0 Then
                connectPoints(mainExit, exits(exits.Count - 1))
            Else
                exits.Add(mainExit)
            End If
            For n = 1 To numExits
                Select Case Int(Rnd() * 2)
                    Case 0
                        exits.Add(New Point(RoomPos.X + 2, Int(Rnd() * (RoomSpanY - RoomPos.Y)) + RoomPos.Y))
                        If exits.Last.X - 1 < mBoardWidth And exits.Last.X - 1 > 0 And exits.Last.Y - 1 < mBoardHeight And exits.Last.Y - 1 > 0 And
                            exits.Last.X < mBoardWidth And exits.Last.X > 0 And exits.Last.Y < mBoardHeight And exits.Last.Y > 0 AndAlso Not mBoard(exits.Last.Y, exits.Last.X - 1).Tag = 2 Then mBoard(exits.Last.Y, exits.Last.X - 1).Tag = 1
                    Case 1
                        exits.Add(New Point(Int(Rnd() * (RoomSpanX - RoomPos.X)) + RoomPos.X, RoomPos.Y + 2))
                        If exits.Last.X - 1 < mBoardWidth And exits.Last.X - 1 > 0 And exits.Last.Y - 1 < mBoardHeight And exits.Last.Y - 1 > 0 And
                            exits.Last.X < mBoardWidth And exits.Last.X > 0 And exits.Last.Y < mBoardHeight And exits.Last.Y > 0 AndAlso Not mBoard(exits.Last.Y - 1, exits.Last.X).Tag = 2 Then mBoard(exits.Last.Y - 1, exits.Last.X).Tag = 1
                End Select
            Next
        Next
        While exits.Count > 1
            Dim r1 As Integer = Int(Rnd() * exits.Count)
            Dim r2 As Integer = Int(Rnd() * exits.Count)
            Dim r3 As Integer = Int(Rnd() * 3)

            ' MsgBox("R1: " & r1 & " R2: " & r2 & " R3: " & r3 & " E count: " & exits.Count)

            If r1 > r2 Or r3 > 0 Then
                makeDeadEnd(exits(r1), exits)
                exits.RemoveAt(r1)
            Else
                If r1 <> r2 Then
                    connectPoints(exits(r1), exits(r2))
                    Dim exit1 As Point = exits(r1)
                    Dim exit2 As Point = exits(r2)
                    If exits.Contains(exit1) Then exits.Remove(exit1)
                    If exits.Contains(exit2) Then exits.Remove(exit2)
                End If
            End If
        End While

        Dim tileCount As Integer = 0
        For yInd As Integer = 0 To mBoardHeight - 1
            For xInd As Integer = 0 To mBoardWidth - 1
                If mBoard(yInd, xInd).Tag > 0 AndAlso mBoard(yInd, xInd).Text = "" Then
                    tileCount += 1
                End If
            Next
        Next

        If tileCount < 4 Then
            Dim timesDug As Integer = 0
            Do While tileCount < 4 'To handle if it needs to keep "digging", in case it couldn't make it big enough with just one iteration
                timesDug += 1
                If timesDug > 5 Then 'If a map was created without any walkable space, get it started
                    Dim tilesBefore As Integer = tileCount
                    For yInd As Integer = 0 To mBoardHeight - 1
                        For xInd As Integer = 0 To mBoardWidth - 1
                            If mBoard(yInd, xInd).Tag = 0 Then
                                mBoard(yInd, xInd).Tag = 1
                                timesDug = 0
                                tileCount += 1
                                Exit For
                            End If
                        Next
                        If tilesBefore < tileCount Then
                            Exit For
                        End If
                    Next
                    timesDug = 0
                End If
                For yInd As Integer = 0 AndAlso tileCount < 4 To mBoardHeight - 1
                    For xInd As Integer = 0 AndAlso tileCount < 4 To mBoardWidth - 1
                        If mBoard(yInd, xInd).Tag > 0 AndAlso mBoard(yInd, xInd).Text = "" Then
                            If yInd - 1 >= 0 AndAlso mBoard(yInd - 1, xInd).Tag = 0 Then
                                mBoard(yInd - 1, xInd).Tag = 1
                                tileCount += 1
                            ElseIf yInd + 1 < mBoardHeight AndAlso mBoard(yInd + 1, xInd).Tag = 0 Then
                                mBoard(yInd + 1, xInd).Tag = 1
                                tileCount += 1
                            ElseIf xInd - 1 >= 0 AndAlso mBoard(yInd, xInd - 1).Tag = 0 Then
                                mBoard(yInd, xInd - 1).Tag = 1
                                tileCount += 1
                            ElseIf xInd + 1 < mBoardWidth AndAlso mBoard(yInd, xInd + 1).Tag = 0 Then
                                mBoard(yInd, xInd + 1).Tag = 1
                                tileCount += 1
                            End If
                        End If
                    Next
                Next
            Loop
        End If
    End Sub
    Sub makeDeadEnd(ByVal p1 As Point, ByRef exits As List(Of Point))
        'Creates a path from a room to a dead end
        Dim xOry As Boolean = CBool(Int(Rnd() * 2))
        Dim dir As Boolean = CBool(Int(Rnd() * 2))
        For i = 0 To 6
            If xOry Then
                Dim y As Integer
                If dir Then
                    For y = p1.Y To Int(Rnd() * 8)
                        If y < mBoardHeight And y > 0 And p1.X < mBoardWidth And p1.X > 0 AndAlso Not mBoard(y, p1.X).Tag = 2 Then mBoard(y, p1.X).Tag = 1
                    Next
                Else
                    For y = p1.Y To Int(Rnd() * 8) Step -1
                        If y < mBoardHeight And y > 0 And p1.X < mBoardWidth And p1.X > 0 AndAlso Not mBoard(y, p1.X).Tag = 2 Then mBoard(y, p1.X).Tag = 1
                    Next
                End If
                p1 = New Point(p1.X, y)
            Else
                Dim x As Integer
                If dir Then
                    For x = p1.X To Int(Rnd() * 8)
                        If x < mBoardWidth And x > 0 And p1.Y < mBoardHeight And p1.Y > 0 AndAlso Not mBoard(p1.Y, x).Tag = 2 Then mBoard(p1.Y, x).Tag = 1
                    Next
                Else
                    For x = p1.X To Int(Rnd() * 8) Step -1
                        If x < mBoardWidth And x > 0 And p1.Y < mBoardHeight And p1.Y > 0 AndAlso Not mBoard(p1.Y, x).Tag = 2 Then mBoard(p1.Y, x).Tag = 1
                    Next
                End If
                p1 = New Point(x, p1.Y)
            End If
            Dim cont As Integer = (Int(Rnd() * 35))
            If cont = 11 Or cont = 27 Then exits.Add(p1)
            If cont < 8 Then Exit For
        Next
    End Sub
    'floor 7
    Sub placeFloor7Statues()
        For i = 1 To 6
            statueList.Add(New Statue(randPoint(), "Fox", "You see here a statue of a fox"))
        Next

        statueList.Add(New Statue(randPoint(), "seventailsstatue", "You see here a golden statue of a fox"))
    End Sub
    'floor 8
    Sub genFloor8()
        Dim floorLayout As String() = {"##############################_____________________________________",
                                       "#____________________________#_###_______________________________#_",
                                       "#_##############_#############_#@#############################___#_",
                                       "#_#____________#_#___________#_###_#_________________________#####_",
                                       "#_#___####_____#_###_###_###_#_____#___________#############_#_#_#_",
                                       "#_#___########_#_#_#_#_#####_#################_#___________#_#_#_#_",
                                       "#_#___####___#_#_#_#_#___###_________________#_#_#########_#_#_#_#_",
                                       "#_#__________#_#_#_#_#_____#########_#######_#_#_#__######_#_#_#_#_",
                                       "#_############_#_#_###__________#__#_#_____#_#_#_#_________#_#_#_#_",
                                       "#______________#_#______#######_##_#_#_###_#_#_#_#__#####__#_#_#_#_",
                                       "################_######_#_____#_#__#_#_###_#_#_#_#__#_#_#_##_#_#_#_",
                                       "_________________#____#_#_###_#_##_#_#_###_#_#_#_####_#___#__#_#_#_",
                                       "_#################_##_#_#___#_#____#_#__#__#_#_#____#_#_####_#_#_#_",
                                       "_#_____#____________#_#_#####_######_####__#_#_######___####_#___#_",
                                       "_______#____________#_#____________#_______#_#________######_###_#_",
                                       "____#################_#################################_####_#_#_#_",
                                       "____#________________________________________________________#_#_#_",
                                       "_#__#_########################################################_#_#_",
                                       "_#__#_#________________________________________________________#_#_",
                                       "_####_#_#######_###############__####__####__####__#############_#_",
                                       "____#_#_#_______#_______##_#########################_____________#_",
                                       "_####_#_#__####_#_#####_##_####__####__####__####__#############_#_",
                                       "_#__#_#_#_###_#_#_#___#__________________________________________#_",
                                       "_#__#_#_#_###_#_#_#_#_#_##########################################_",
                                       "_####_#_#_###_#_#_###_#_#_____________________________________#____",
                                       "_####_#_#_____#_#_____#_#_#########_###########################____",
                                       "_####_#_#_#####_###_###_#_#_______#___________________________#____",
                                       "__##__#_###_______#_#___#_#_#####_#_#########################_#____",
                                       "__##__#_#_#######_#_#_#_#_#_#___#___#_______________________#_###__",
                                       "______#_#_________#_#_#_#_#_#_#####_#_#_###################_#___#__",
                                       "______#_###########_#_###_#_#_#####_#_#_#_________________#_#_####_",
                                       "______#_____________#_____#_#_##%##_#_#_#_#################_#_####_",
                                       "______#_#_#########_#_#####_#_#####_#_#_#_#_______#_________#_####_",
                                       "______#_#_#___#_#_#_#_#_____#_#####_#_#_#_#_#####_#_#####_#_#_####_",
                                       "#####_#_###_#___#_#_#_#_###_#_______#_#_#_#___#_#_#_###_#_#_#______",
                                       "#___#_#_#_#_#_#___#_#_#_#_#_#########_#_#_#####_#_#_____#_###______",
                                       "#_#_#_#_#_#_#_#_#___#_#_#_#___________#_#_______#_#######_#_#______",
                                       "#_#_#_#_#_###_#_###_#_#_#_#############_#_#####_#_________#_#______",
                                       "#_#_#_#_____###_#_#_#_#_#_______________#_____#_###########_#______",
                                       "#_#_#_###_____#_#_#_#_#_#################_____#_____________#______",
                                       "#_#_#_#_#####_###_#_#_#___#___________________#############_#______",
                                       "#_#_###_____#_____#_###___#####___________________________#_#______",
                                       "__#_#_#_###_#######_______#___#_#########################_#_###____",
                                       "###_#_#___#_______________#_#_#_#___________________________#_#_#__",
                                       "#___#_###_#_###_#########_#_#___#_###########################_#_#__",
                                       "#####_#___#_#_#_#_______#_#_#_###_#___________________________#_#__",
                                       "______#_###_#_#_#_###___#_#_#_#_#_#_###############_####______#_#__",
                                       "______#_#___#_#_#_#####_#_#_#_#_#_#_#_____________#____########_#__",
                                       "______#_#_#_#_#_#_###_#_#_#_#_#_#_#_#_###########_######________#__",
                                       "______#_#_#_#_#_#_____#_#_____#_#_#_#_#_________#________########__",
                                       "______#_#####_#_#######_#_#####_#_#_#_#_#######_#_###########___#__",
                                       "______#_______#_________#_#_____#_#___#_______#_#_#______########__",
                                       "______#_______###########_#######_###_#########_#_#________________",
                                       "_####_#___________________________#_#___________#_#________________",
                                       "_####_#############################_#############_#__#_#_#_#_#_#___",
                                       "_####___#____#____________________________________#__#_#_###_#_#___",
                                       "_########____######################################___#__#_#_###___",
                                       "___________________________________________________________________"}

        If mBoardHeight < 50 Then mBoardHeight = 50
        If mBoardWidth < 69 Then mBoardWidth = 69

        ReDim mBoard(mBoardHeight, mBoardWidth)
        For y = 0 To mBoardHeight
            For x = 0 To mBoardWidth
                mBoard(y, x) = New mTile(0, "", Color.Black)
            Next
        Next

        For y = 0 To UBound(floorLayout)
            Dim line = floorLayout(y).ToCharArray
            For x = 0 To UBound(line)
                If Not line(x) = "_"c Then mBoard(y, x).Tag = 2
                If line(x) = "%"c Then
                    stairs = New Point(x, y)
                ElseIf line(x) = "@"c Then
                    Game.player1.pos = New Point(x, y)
                End If
            Next
        Next

        placeChest(floorCode)
        placeTraps()
    End Sub
    'floor 9
    Sub genFloor9()
        Dim floorLayout As String() = {"___________________________________##✢*##___________",
                                       "___________________________________#✢*✢*#___________",
                                       "___________________________________*%⇨⇦#✢___________",
                                       "___________________________________##><##___________",
                                       "___________________________________##>⇦✢*___________",
                                       "___________________________________##⇨<*#___________",
                                       "___________________________________##><##___________",
                                       "___________________________________#✢>⇦*#___________",
                                       "___________________________________#*><##___________",
                                       "___________________________________##⇨<##___________",
                                       "_____________________##____________##><##___________",
                                       "___________________#####___________#*⇨<#✢___________",
                                       "_____________###########___________##>⇦##___________",
                                       "___________________#####___________##><*#___________",
                                       "____________________###____________##⇨⇦##___________",
                                       "_____________________#_____________#*⇨<##___________",
                                       "____#####____________#_____________##>⇦##___________",
                                       "____#####_____###____#_____________✢#⇨<##___________",
                                       "____#####____########################><##___________",
                                       "____#####___###__##_______________###>⇦#*___________",
                                       "_######_____##____###########_____###\/##___________",
                                       "_######____###______#_______###########✢#___________",
                                       "_#############______##_____________######___________",
                                       "_##@#########______###______________________________",
                                       "_######____________###______________________________",
                                       "____________________________________________________",
                                       "____________________________________________________"}
        If mBoardHeight < 27 Then mBoardHeight = 27
        If mBoardWidth < 53 Then mBoardWidth = 53

        ReDim mBoard(mBoardHeight, mBoardWidth)
        For y = 0 To mBoardHeight
            For x = 0 To mBoardWidth
                mBoard(y, x) = New mTile(0, "", Color.Black)
            Next
        Next

        For y = 0 To 26
            Dim line = floorLayout(y).ToCharArray
            For x = 0 To UBound(line)
                If Not line(x) = "_"c Then mBoard(y, x).Tag = 2
                If line(x) = "%"c Then
                    stairs = New Point(x, y)
                ElseIf line(x) = "@"c Then
                    Game.player1.pos = New Point(x, y)
                ElseIf line(x) = "✢"c Then
                    mBoard(y, x).Text = "✢"
                ElseIf line(x) = "*"c Then
                    mBoard(y, x).Text = "*"
                ElseIf line(x) = "⇨"c Then
                    mBoard(y, x).Text = "⇨"
                    mBoard(y, x).Tag = 0
                ElseIf line(x) = "⇦"c Then
                    mBoard(y, x).Text = "⇦"
                    mBoard(y, x).Tag = 0
                ElseIf line(x) = ">"c Then
                    mBoard(y, x).Text = ">"
                    mBoard(y, x).Tag = 0
                ElseIf line(x) = "<"c Then
                    mBoard(y, x).Text = "<"
                    mBoard(y, x).Tag = 0
                ElseIf line(x) = "\"c Then
                    mBoard(y, x).Text = "\"
                    mBoard(y, x).Tag = 0
                ElseIf line(x) = "/"c Then
                    mBoard(y, x).Text = "/"
                    mBoard(y, x).Tag = 0
                End If
            Next
        Next

        placeChest(floorCode, Int(Rnd() * 3) + 4)
        placeTraps()
    End Sub

    '|-Boss Hallways-|
    Sub genBossFloor(ByRef p As Player)
        'Creates a straight hallway of a floor for a boss floor
        If mBoardHeight < 30 Then mBoardHeight = 30
        If mBoardWidth < 15 Then mBoardWidth = 15
        For y = 0 To 25
            For x = 3 To 7
                mBoard(y, x).Tag = 2
            Next
        Next
        p.pos = New Point(5, 25)
        stairs = New Point(5, 2)
        If floorNumber = 5 Then genMedusaStatues()
        'beatBoss = True
    End Sub
    'floor 5
    Sub genMedusaStatues()
        'places the statues on floor 5 for ambience
        Randomize()
        Dim numStatues As Integer = Int((Rnd() * 5) + 6)
        For i = 0 To numStatues
            Dim x = Int((Rnd() * 4) + 3)
            Dim y = Int((Rnd() * 15) + 3)
            Dim tr As New Monster()
            tr.pos = New Point(x, y)
            statueList.Add(New Statue(tr))
        Next
    End Sub

    '|-Space Floor-|
    Sub genSpaceFloor()
        Dim floorLayout As String() = {"____________#####____________",
                                       "___________#######___________",
                                       "___________###%###___________",
                                       "___________#######___________",
                                       "____________#####____________",
                                       "__#^##________#______________",
                                       "_######_______#______####____",
                                       "_$#############_____##&###___",
                                       "_######_______############___",
                                       "__##^#________#_____###&##___",
                                       "______________#______####____",
                                       "______________#______________",
                                       "_____________##!_____________",
                                       "_____________#@#_____________",
                                       "_____________###_____________"}

        If mBoardHeight < 15 Then mBoardHeight = 15
        If mBoardWidth < 30 Then mBoardWidth = 30

        ReDim mBoard(mBoardHeight, mBoardWidth)
        For y = 0 To mBoardHeight
            For x = 0 To mBoardWidth
                mBoard(y, x) = New mTile(0, "", Color.Black)
            Next
        Next

        For y = 0 To 14
            Dim line = floorLayout(y).ToCharArray
            For x = 0 To UBound(line)
                If Not line(x) = "_"c Then mBoard(y, x).Tag = 1
                If line(x) = "%"c Then
                    stairs = New Point(x, y)
                ElseIf line(x) = "^"c Then
                    Dim chestPoint = New Point(x, y)
                    genSpaceChest2(chestPoint)
                ElseIf line(x) = "&"c Then
                    Dim chestPoint = New Point(x, y)
                    genSpaceChest1(chestPoint)
                ElseIf line(x) = "$"c Then
                    Dim trapPoint = New Point(x, y)
                    Dim t = Trap.trapFactory(CStr(x) & "*" & CStr(y) & "*" & CStr(6) & "*")
                    trapList.Add(t)
                    mBoard(trapPoint.Y, trapPoint.X).ForeColor = Color.FromArgb(45, 45, 45)
                    mBoard(trapPoint.Y, trapPoint.X).Text = "+"
                ElseIf line(x) = "!"c Then
                    Dim trapPoint = New Point(x, y)
                    Dim t = Trap.trapFactory(CStr(x) & "*" & CStr(y) & "*" & CStr(5) & "*")
                    trapList.Add(t)
                    mBoard(trapPoint.Y, trapPoint.X).ForeColor = Color.FromArgb(45, 45, 45)
                    mBoard(trapPoint.Y, trapPoint.X).Text = "+"
                ElseIf line(x) = "@"c Then
                    Game.player1.pos = New Point(x, y)
                End If
            Next
        Next
    End Sub
    Sub genSpaceChest1(ByVal p As Point)
        Dim c1 As Chest
        Dim inv = New Inventory(False)

        Dim r = 0
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("Photon_Armor", r)
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("Labcoat", r)
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("Mobile_Powerbank", r)
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("Discharge_Gauntlets", r)
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("Photon_Blade", r)
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("BitGold", r)

        inv.add("Space_Age_Jumpsuit", 1)
        c1 = DDConst.BASE_CHEST.Create(inv, p, False)

        chestList.Add(c1)
        mBoard(p.Y, p.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(p.Y, p.X).Text = "#"
    End Sub
    Sub genSpaceChest2(ByVal p As Point)
        Dim c1 As Chest
        Dim inv = New Inventory(False)

        Dim r = 0
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("Shrink_Ray", r)
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("Galaxy_Dye", r)
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("CryoGrenade", r)
        If Int(Rnd() * 3) = 0 Then r = 1 Else r = 0
        inv.add("Combat_Module", r)

        inv.add("Vial_of_BIM_II", 1)
        c1 = DDConst.BASE_CHEST.Create(inv, p, False)

        chestList.Add(c1)
        mBoard(p.Y, p.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(p.Y, p.X).Text = "#"
    End Sub

    '|-Space Floor 2-|
    Sub genSpaceFloor2()
        Dim floorLayout As String() = {"_________________________________________________________________________________________",
                                       "_________________________________________________________________________________________",
                                       "_________________________________________________________________________________________",
                                       "_____________________________________________###_________________________________________",
                                       "__________________###_______________________#####________________________###_____________",
                                       "__________________#########################|##%##|######################|###_____________",
                                       "__________________###_______________________#####________________________###_____________",
                                       "__________________###________________________###_________________________###_____________",
                                       "__________________###____________________________________________________###_____________",
                                       "__________________###______LLL___________________________LLL_____________###_____________",
                                       "__________________########|LLL___________________________LLLLLLLLLLLLLLLL###_____________",
                                       "__________________###______LLL___________________________LLL_____________###_____________",
                                       "__________________###____________________________________________________###_____________",
                                       "__________________###______###___________________________###_____________###_____________",
                                       "__________________##########$#___________________________#!#################_____________",
                                       "_________###______###______###___________________________###_____________###_____________",
                                       "________#####_____###__________________####______________________________###_____________",
                                       "________#&T######|###______LLL_________#^^#______________###_____________###_____________",
                                       "________#####_____###LLLLLLLLL_________####______________###################_____________",
                                       "_________###______###______LLL__________#________________###_____________###_____________",
                                       "__________________###___________________#________________________________###_____________",
                                       "__________________###___________________#________________________________###_____________",
                                       "LLLLLLLLLLLLLLLLL|######################################################|###|LLLLLLLLLLLL",
                                       "LLLLLLLLLLLLLLLLL|######################################################|###|LLLLLLLLLLLL",
                                       "LLLLLLLLLLLLLLLLL|######################################################|###|LLLLLLLLLLLL",
                                       "LLLLLLLLLLLLLLLLL|######################################################|###|LLLLLLLLLLLL",
                                       "__________________---____________________________________________________###_____________",
                                       "__________________###____________________________________________________###_____________",
                                       "__________________###|LLLLLLLLLLLLLL____________________LLLLLLLLLLLLLLLL|###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###____________________________________________________###_____________",
                                       "__________________###|LLLLLLLLLLLLLL____________________####################_____________",
                                       "__________________###__________LLLLL____________________#####____________###_____________",
                                       "__________________###__________LLLLL____________________###@#____________###_____________",
                                       "__________________###__________LLLLL____________________#####____________###_____________",
                                       "__________________###____________________________________________________###_____________",
                                       "__________________###|LLLLLLLLLLLLLL____________________LLLLLLLLLLLLLLLL|###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###____________________________________________________###_____________",
                                       "__________________###|LLLLLLLLLLLLLL____________________LLLLLLLLLLLLLLLL|###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###__________LLLLL____________________LLLLL____________###_____________",
                                       "__________________###____________________________________________________###_____________",
                                       "__________________---____________________________________________________---_____________",
                                       "__________________LLL____________________________________________________LLL_____________",
                                       "__________________LLL____________________________________________________LLL_____________",
                                       "__________________LLL____________________________________________________LLL_____________",
                                       "__________________LLL____________________________________________________LLL_____________",
                                       "__________________LLL____________________________________________________LLL_____________",
                                       "__________________LLL____________________________________________________LLL_____________",
                                       "__________________LLL____________________________________________________LLL_____________"}

        If mBoardHeight < 60 Then mBoardHeight = 60
        If mBoardWidth < 100 Then mBoardWidth = 100

        ReDim mBoard(mBoardHeight, mBoardWidth)
        For y = 0 To mBoardHeight
            For x = 0 To mBoardWidth
                mBoard(y, x) = New mTile(0, "", Color.Black)
            Next
        Next

        For y = 0 To UBound(floorLayout)
            Dim line = floorLayout(y).ToCharArray
            For x = 0 To UBound(line)
                If Not line(x) = "_"c Then mBoard(y, x).Tag = 2
                If line(x) = "L"c Then
                    mBoard(y, x).Tag = 1
                ElseIf line(x) = "%"c Then
                    stairs = New Point(x, y)
                ElseIf line(x) = "^"c Then
                    Dim chestPoint = New Point(x, y)
                    genSpaceChest3(chestPoint)
                ElseIf line(x) = "&"c Then
                    Dim chestPoint = New Point(x, y)
                    genSpaceChest4(chestPoint)
                ElseIf line(x) = "$"c Then
                    Dim trapPoint = New Point(x, y)
                    Dim t = Trap.trapFactory(CStr(x) & "*" & CStr(y) & "*" & CStr(8) & "*")
                    trapList.Add(t)
                    mBoard(trapPoint.Y, trapPoint.X).ForeColor = Color.FromArgb(45, 45, 45)
                    mBoard(trapPoint.Y, trapPoint.X).Text = "+"
                ElseIf line(x) = "!"c Then
                    Dim trapPoint = New Point(x, y)
                    Dim t = Trap.trapFactory(CStr(x) & "*" & CStr(y) & "*" & CStr(7) & "*")
                    trapList.Add(t)
                    mBoard(trapPoint.Y, trapPoint.X).ForeColor = Color.FromArgb(45, 45, 45)
                    mBoard(trapPoint.Y, trapPoint.X).Text = "+"
                ElseIf line(x) = "@"c Then
                    Game.player1.pos = New Point(x, y)
                ElseIf line(x) = "-"c Then
                    mBoard(y, x).Tag = 0
                    mBoard(y, x).Text = "-"
                ElseIf line(x) = "|"c Then
                    mBoard(y, x).Tag = 0
                    mBoard(y, x).Text = "|"
                ElseIf line(x) = "T"c Then
                    addNPC(Game.ttraveler, New Point(x, y))
                ElseIf line(x) = "@"c Then
                    Game.ttraveler.pos = New Point(x, y)
                End If
            Next
        Next
    End Sub
    Sub genSpaceChest3(ByVal p As Point)
        Dim c1 As Chest
        Dim inv = New Inventory(False)

        Dim r = 0

        If Int(Rnd() * 6) = 0 Then r = Int(Rnd() * 3) + 1 Else r = 0
        inv.add("BitGold", r)

        If Int(Rnd() * 8) = 0 Then r = 1 Else r = 0
        inv.add("Galaxy_Dye", r)

        If Int(Rnd() * 6) = 0 Then r = Int(Rnd() * 5) + 1 Else r = 0
        inv.add("CryoGrenade", r)

        If Int(Rnd() * 8) = 0 Then r = 1 Else r = 0
        inv.add("Photon_Armor", r)

        If Int(Rnd() * 8) = 0 Then r = 1 Else r = 0
        inv.add("Vial_of_BIM_II", r)

        If Int(Rnd() * 8) = 0 Then r = 1 Else r = 0
        inv.add("Mobile_Powerbank", r)

        If Int(Rnd() * 8) = 0 Then r = 1 Else r = 0
        inv.add("Discharge_Gauntlets", r)

        If Int(Rnd() * 8) = 0 Then r = 1 Else r = 0
        inv.add("Photon_Blade", r)

        If Int(Rnd() * 2) = 0 Then
            Select Case Int(Rnd() * 3)
                Case 0
                    inv.add("Phase_Rifle", 1)
                Case 1
                    inv.add("Phase_Hammer", 1)
                Case 2
                    inv.add("Phase_Drill", 1)
            End Select
        Else
            Select Case Int(Rnd() * 4)
                Case 0
                    inv.add("Paleomancer's_Diary", 1)
                Case 1
                    inv.add("Marissa's_Notes", 1)
                Case 2
                    inv.add("AAAAAA_Specification", 1)
                Case 3
                    inv.add("BitGold", 1)
            End Select
        End If

        inv.add("AAAAAA_Battery", CInt(Rnd() * 10) + 5)

        c1 = DDConst.BASE_CHEST.Create(inv, p, False)

        chestList.Add(c1)
        mBoard(p.Y, p.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(p.Y, p.X).Text = "#"
    End Sub
    Sub genSpaceChest4(ByVal p As Point)
        Dim c1 As Chest
        Dim inv = New Inventory(False)

        inv.add("Phase_Pistol", 1)
        inv.add("Phase_Deflector", 1)
        inv.add("Phase_Vibrator", 1)
        inv.add("AAAAAA_Battery", CInt(Rnd() * 10) + 5)

        c1 = DDConst.BASE_CHEST.Create(inv, p, False)

        chestList.Add(c1)
        mBoard(p.Y, p.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(p.Y, p.X).Text = "#"
    End Sub

    '|-Legacy Floor-|
    Sub genLegacyFloor()
        Dim floorLayout As String() = {"_############################",
                                       "____####____________#@#_____#",
                                       "____####____________###_____#",
                                       "____####____________________#",
                                       "#############################",
                                       "#___________########_________",
                                       "#___________###########______",
                                       "#_____________###__#####______",
                                       "#############################",
                                       "______#####___###__#####____#",
                                       "______#####___###__#####____#",
                                       "______##^##___###___________#",
                                       "#############################",
                                       "#_____#####___###____________",
                                       "#_____________#########______",
                                       "#_____________#########______",
                                       "##################%##########",
                                       "______________#########_____#",
                                       "______________#########______"}

        If mBoardHeight < 20 Then mBoardHeight = 20
        If mBoardWidth < 30 Then mBoardWidth = 30

        For y = 0 To 18
            Dim line = floorLayout(y).ToCharArray
            For x = 0 To UBound(line)
                If Not line(x) = "_"c Then mBoard(y, x).Tag = 1
                If line(x) = "%"c Then
                    stairs = New Point(x, y)
                ElseIf line(x) = "^"c Then
                    Dim chestPoint = New Point(x, y)
                    genLegacyChest(chestPoint)
                ElseIf line(x) = "@"c Then
                    Game.player1.pos = New Point(x, y)
                End If
            Next
        Next
    End Sub
    Sub genLegacyChest(ByVal p As Point)
        Dim c1 As Chest
        Dim inv = New Inventory(False)

        inv.add("Chicken_Suit", 1)
        inv.add("Bunny_Ears", 1)
        inv.add(150, 1)

        c1 = DDConst.BASE_CHEST.Create(inv, p, False)

        chestList.Add(c1)
        mBoard(p.Y, p.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(p.Y, p.X).Text = "#"
    End Sub

    '|---OBJECT PLACEMENT---|
    Sub placeStairs()
        stairs = randPoint()
        mBoard(stairs.Y, stairs.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(stairs.Y, stairs.X).Text = "H"
    End Sub
    Sub placePlayer(ByRef p As Player)
        p.pos = getStartPlayerPos()
        playerPosition = p.pos
        mBoard(p.pos.Y, p.pos.X).Text = "@"
        If floorNumber = 4 Then placeFloor4TrappedChest(Game.player1)
    End Sub
    Public Function getStartPlayerPos() As Point
        Select Case floorNumber
            Case 5, 75
                Return New Point(5, 25)
            Case 9
                Return New Point(4, 24)
            Case 91017
                Return New Point(21, 1)
            Case 9999
                Return New Point(14, 13)
            Case 10000
                Return New Point(59, 35)
            Case Else
                Return randPoint()
        End Select
    End Function
    Sub placeChest(ByVal code As String, Optional ByVal numChests As Integer = 0)
        'Fill Chest Tier List
        For i = 1 To DDConst.BASE_CHEST.tiers.Count - 1
            DDConst.BASE_CHEST.tiers(i).Clear()
        Next
        For i = 0 To DDConst.BASE_CHEST.contents.upperBound
            Dim c_item = DDConst.BASE_CHEST.contents.item(i)
            If c_item.getTier() <> Nothing And Not c_item.droppable Then
                DDConst.BASE_CHEST.tiers(c_item.getTier()).Add(c_item)
            End If
        Next
        Rnd(-1)
        Randomize(code.GetHashCode)
        'Dim numChests As Integer = CInt(Int(Rnd() * 8) + 3) * Int((mBoardWidth / 30) + (mBoardHeight / 30) / 2)
        If numChests = 0 Then numChests = CInt((Int(Rnd() * Game.chestFreqRange) + Game.chestFreqMin) * (Math.Sqrt(coveredBoardSpace) / Game.chestSizeDependence))

        If floorNumber = 3 Then
            numChests *= 1.5
            placeKeyChest()
        End If


        If floorNumber >= 3 And Int(Rnd() * 20) = 0 Then
            Dim p = randPoint()
            addChest(New LoadedChest(p, 5), p)
        End If

        If Not Game.player1 Is Nothing AndAlso Game.player1.quests(qInds.dfaUpgrade).getActive Then
            For i = 0 To Int(Rnd() * 3) + 1
                Dim p = randPoint()
                addChest(New LoadedChest(p, 6), p)
            Next
        End If

        For i = 1 To numChests
            Dim chestPoint = randPoint()
            Dim chest As Chest = DDConst.BASE_CHEST.Create(chestPoint, code)
            addChest(chest, chestPoint)
        Next


        For Each c In chestList
            mBoard(c.pos.Y, c.pos.X).Text = ""
        Next
    End Sub
    Sub addChest(ByVal c As Chest, ByVal p As Point)
        chestList.Add(c)
        mBoard(p.Y, p.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(p.Y, p.X).Text = "#"
    End Sub
    Sub placeTraps()
        trapList.Clear()
        If Game.trapSizeDependence <= 0 Then Game.trapSizeDependence = 1
        Dim numtrap As Integer = CInt(Int(Rnd() * Game.trapFreqRange) + Game.trapFreqMin) * (Math.Sqrt(coveredBoardSpace) / Game.trapSizeDependence)
        For i = 1 To numtrap
            Dim trapPoint = randPoint()
            mBoard(trapPoint.Y, trapPoint.X).ForeColor = Color.FromArgb(45, 45, 45)
            mBoard(trapPoint.Y, trapPoint.X).Text = "+"
            Dim t = Trap.trapFactory(Trap.getRandomTrapId(), trapPoint)
            trapList.Add(t)
        Next
    End Sub
    Sub placeNPCs(ByRef npc_list As List(Of ShopNPC), ByVal possibleNPCs As Integer())
        npcPositions.Clear()

        Dim numNpc As Integer = Int(Rnd() * possibleNPCs.Length) + 1
        If floorNumber = 1 Then numNpc = 1
        Dim placed = New List(Of Integer)

        For i = 1 To numNpc
            Dim npcPoint = randPoint()
            Dim npcInd = Int(Rnd() * possibleNPCs.Length)
            While placed.Contains(npcInd) And Not placed.Count >= npc_list.Count
                npcInd = Int(Rnd() * possibleNPCs.Length)
            End While

            If floorNumber = 1 Then npcInd = 0

            Dim sNPC = npc_list(possibleNPCs(npcInd))

            addNPC(sNPC, npcPoint)

            If floorNumber = 3 Then sNPC.inv.add(53, 1) Else sNPC.inv.item(53).count = 0
            placed.Add(npcInd)
        Next

        If Game.player1.cursed And Game.cbrok.pos.X = -1 And Not Game.cbrok.isDead Then addNPC(Game.cbrok, randPoint)

        For i = 0 To npc_list.Count - 1
            npcPositions.Add(npc_list(i).pos)
        Next
    End Sub
    Function getPossibleNPCs() As Integer()
        If floorNumber < 3 Then
            Return {sNPCInd.shopkeeper, sNPCInd.shadywizard, sNPCInd.foodvendor}
        ElseIf floorNumber = 3 Then
            Return {sNPCInd.shopkeeper, sNPCInd.shadywizard, sNPCInd.hypnoteach, sNPCInd.foodvendor, sNPCInd.cursebroker}
        ElseIf floorNumber = 7 Then
            Return {sNPCInd.hypnoteach}
        ElseIf floorNumber = 13 Then
            Return {sNPCInd.foodvendor, sNPCInd.cursebroker}
        Else
            If Int(Rnd() * 2) = 0 And Game.player1.className.StartsWith("Magical") Then
                Return {sNPCInd.shopkeeper, sNPCInd.shadywizard, sNPCInd.hypnoteach, sNPCInd.weaponsmith, sNPCInd.cursebroker, sNPCInd.maskmaggirl}
            Else
                Return {sNPCInd.shopkeeper, sNPCInd.shadywizard, sNPCInd.hypnoteach, sNPCInd.foodvendor, sNPCInd.weaponsmith, sNPCInd.cursebroker}
            End If
        End If
    End Function
    Sub addNPC(ByRef n As NPC, ByRef npcPoint As Point)
        n.pos = npcPoint
        mBoard(npcPoint.Y, npcPoint.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(npcPoint.Y, npcPoint.X).Text = "$"
    End Sub
    Sub placeKeyChest()
        Dim ChestP = randPoint()
        Dim c As Chest = New LoadedChest(ChestP, 3)
        chestList.Add(c)
        mBoard(c.pos.Y, c.pos.X).ForeColor = Color.FromArgb(45, 45, 45)
        mBoard(c.pos.Y, c.pos.X).Text = "#"
    End Sub


    '|---UTILITY METHODS---|
    Function randPoint() As Point
        Dim posX As Integer
        Dim posY As Integer
        Dim attempts = 0
        Do While (mBoard(posY, posX).Tag < 1 Or mBoard(posY, posX).Text <> "") And attempts < 75
            posX = CInt(Int(Rnd() * mBoardWidth))
            posY = CInt(Int(Rnd() * mBoardHeight))
            attempts += 1
        Loop
        Return New Point(posX, posY)
    End Function
    Function route(ByVal p1 As Point, ByVal p2 As Point) As Point()
        'Generates a path between two points.  This is not always the shortest path
        'between the two points, but they will always be connected.

        'iterative dijkstra's shortest path implementation
        Dim dist(mBoardHeight, mBoardWidth) As Integer
        Dim allPoints As List(Of Point) = New List(Of Point)
        Dim prev(mBoardHeight, mBoardWidth) As Point
        Dim path As List(Of Point) = New List(Of Point)
        For i = 0 To mBoardHeight - 1
            For j = 0 To mBoardWidth - 1
                dist(i, j) = 999999999
                allPoints.Add(New Point(j, i))
                prev(i, j) = Nothing
            Next
        Next
        dist(p1.Y, p1.X) = 0
        While allPoints.Count > 0
            Dim min = allPoints(0)
            For i = 0 To allPoints.Count - 1
                If dist(allPoints(i).Y, allPoints(i).X) < dist(min.Y, min.X) Then min = allPoints(i)
            Next
            allPoints.Remove(min)
            Dim u, d, l, r As Point
            u = New Point(min.X - 1, min.Y)
            d = New Point(min.X + 1, min.Y)
            l = New Point(min.X, min.Y - 1)
            r = New Point(min.X, min.Y + 1)
            For Each p In {u, d, l, r}
                Dim tDist = dist(min.Y, min.X) + DDUtils.distance(min, p)
                If Not (p.X < 0 Or p.X > mBoardWidth - 1 Or p.Y < 0 Or p.Y > mBoardHeight - 1) AndAlso Not mBoard(p.Y, p.X).Tag = 0 AndAlso Not path.Contains(p) AndAlso allPoints.Contains(p) Then
                    If tDist < dist(p.Y, p.X) Then
                        dist(p.Y, p.X) = tDist
                        prev(p.Y, p.X) = min
                    End If
                End If
                If p.Equals(p2) Then
                    Dim pp = p2
                    While Not path.Contains(pp)
                        path.Insert(0, pp)
                        pp = prev(pp.Y, pp.X)
                    End While
                    Exit For
                End If
            Next
        End While
        path.RemoveAt(0)
        Return path.ToArray
    End Function
    Sub printBoard()
        'Outputs a file creating a text version of the board
        Dim writer As IO.StreamWriter
        writer = IO.File.CreateText("bo.ard")
        For y = 0 To mBoardHeight - 1
            Dim line As String = ""
            For x = 0 To mBoardWidth - 1
                Select Case mBoard(y, x).Tag
                    Case 0
                        line += " "
                    Case Else
                        If Not mBoard(y, x).Text = "" Then line += mBoard(y, x).Text Else line += "_"
                End Select
            Next
            writer.WriteLine(line)
        Next
        writer.Flush()
        writer.Close()
    End Sub
    Shared Function genRNDLVLCode() As String
        'returns a randomized seed to be used in level generation
        Dim numLetters As String = "abcdefghijklmnopqrstuvwxyz123456789"
        Dim output As String = ""
        Randomize() 'initializes the randomizer
        For i = 0 To 8
            output += numLetters.Substring(Int(Rnd() * numLetters.Length), 1)
        Next
        Return output
    End Function
    Function getTile(ByVal y As Integer, ByVal x As Integer) As mTile
        Return mBoard(y, x)
    End Function
    Sub verifyNoDisconectedChunks(ByRef p As Player)
        connectPoints(p.pos, stairs)
    End Sub


    '|---SERIALIZATION METHODS---|
    Function saveMFloor() As String
        Dim out = "floornumber" & floorNumber & "%"

        out += floorNumber & "%"                '1
        out += floorCode & "%"                  '2
        out += mBoardHeight & "%"               '3
        out += mBoardWidth & "%"                '4

        out += "traps%"
        out += trapList.Count - 1 & "%"         '6
        For i = 0 To trapList.Count - 1
            out += trapList(i).ToString & "%"   '7 to 6 + traplist.Count
        Next

        out += "statues%"
        out += statueList.Count - 1 & "%"       '8 + traplist.Count
        For i = 0 To statueList.Count - 1
            out += statueList(i).toString & "%" '9 + traplist.Count to 8 + traplist.Count + statueList.Count
        Next

        out += "chest%"
        out += chestList.Count - 1 & "%"        '10 + traplist.Count + statueList.Count
        For i = 0 To chestList.Count - 1
            out += chestList(i).ToString & "%"  '11 + traplist.Count + statueList.Count to 10 + traplist.Count + statueList.Count + chestList.Count
        Next

        out += "beatboss%"
        out += beatBoss & "%"                   '12 + traplist.Count + statueList.Count + chestList.Count

        out += "stairs%"
        out += stairs.X & "%"                   '14 + traplist.Count + statueList.Count + chestList.Count
        out += stairs.Y & "%"                   '15 + traplist.Count + statueList.Count + chestList.Count

        out += "playerpos%"
        out += playerPosition.X & "%"           '17 + traplist.Count + statueList.Count + chestList.Count
        out += playerPosition.Y & "%"           '18 + traplist.Count + statueList.Count + chestList.Count

        out += "NPCpos%"
        out += CStr(npcPositions.Count - 1) & "%" '20 + traplist.Count + statueList.Count + chestList.Count
        For i = 0 To npcPositions.Count - 1
            out += npcPositions(i).X & "~"
            out += npcPositions(i).Y & "%"      '21 + traplist.Count + statueList.Count + chestList.Count to 20 + traplist.Count + statueList.Count + chestList.Count + npcPositions.Count
        Next

        out += "sessions%"
        out += CStr(sessions.Count - 1) & "%"   '21 + traplist.Count + statueList.Count + chestList.Count + npcPositions.Count
        For i = 0 To sessions.Count - 1
            out += sessions.Values(i).ToString() & "%" '22 + traplist.Count + statueList.Count + chestList.Count + npcPositions.Count to 21 + traplist.Count + statueList.Count + chestList.Count + npcPositions.Count + sessions.Count
        Next

        out += "boardtags%"
        For y = 0 To mBoardHeight - 1
            For x = 0 To mBoardWidth - 1
                out += mBoard(y, x).Tag & "%"   '23 + traplist.Count + statueList.Count + chestList.Count + npcPositions.Count + sessions.Count
            Next
        Next

        Return out
    End Function
    Sub loadMFloor(ByVal s As String)
        Dim buffer = s.Split("%")

        floorNumber = CInt(buffer(1))
        floorCode = buffer(2)
        mBoardHeight = CInt(buffer(3))
        mBoardWidth = CInt(buffer(4))

        'reset the board
        ReDim mBoard(mBoardHeight, mBoardWidth)
        For y = 0 To mBoardHeight
            For x = 0 To mBoardWidth
                mBoard(y, x) = New mTile(0, "", Color.Black)
            Next
        Next

        trapList.Clear()
        For i = 0 To CInt(buffer(6))
            Dim t = Trap.trapFactory(buffer(7 + i))
            trapList.Add(t)
            If t.pos.X < mBoardWidth And t.pos.X > 0 And t.pos.Y < mBoardHeight And t.pos.Y > 0 Then
                mBoard(t.pos.Y, t.pos.X).Text = "+"
            End If
        Next

        statueList.Clear()
        For i = 0 To CInt(buffer(8 + trapList.Count))
            statueList.Add(New Statue(buffer(9 + trapList.Count + i)))
        Next

        chestList.Clear()
        For i = 0 To CInt(buffer(10 + trapList.Count + statueList.Count))
            chestList.Add(New Chest().Create(buffer(11 + trapList.Count + statueList.Count + i)))
        Next

        beatBoss = CBool(buffer(12 + trapList.Count + statueList.Count + chestList.Count))

        stairs = New Point(CInt(buffer(14 + trapList.Count + statueList.Count + chestList.Count)),
                           CInt(buffer(15 + trapList.Count + statueList.Count + chestList.Count)))

        playerPosition = New Point(CInt(buffer(17 + trapList.Count + statueList.Count + chestList.Count)),
                           CInt(buffer(18 + trapList.Count + statueList.Count + chestList.Count)))

        npcPositions.Clear()
        For i = 0 To CInt(buffer(20 + trapList.Count + statueList.Count + chestList.Count))
            Dim xy = buffer(21 + trapList.Count + statueList.Count + chestList.Count + i).Split("~")
            npcPositions.Add(New Point(xy(0), xy(1)))
        Next

        Dim sessionLines = 0
        If buffer(21 + trapList.Count + statueList.Count + chestList.Count + npcPositions.Count).Equals("sessions") Then sessionLines = 2

        If sessionLines = 2 Then
            sessions.Clear()
            For i = 0 To CInt(buffer(22 + trapList.Count + statueList.Count + chestList.Count + npcPositions.Count))
                Dim idxybb = buffer(23 + trapList.Count + statueList.Count + chestList.Count + npcPositions.Count + i).Split("~")
                sessions.Add(CInt(idxybb(0)), New Session(idxybb(0), New Point(idxybb(1), idxybb(2)), idxybb(3)))
            Next
        End If

        coveredBoardSpace = 0
        For y = 0 To mBoardHeight - 1
            For x = 0 To mBoardWidth - 1
                Dim i = (y * mBoardWidth) + x
                mBoard(y, x).Tag = CInt(buffer(22 + sessionLines + trapList.Count + statueList.Count + chestList.Count + npcPositions.Count + sessions.Count + i))
                If mBoard(y, x).Tag > 0 Then coveredBoardSpace += 1
            Next
        Next

        '|Floor statue doublecheck|
        If floorNumber = 7 And Game.player1.perks(perk.seventailsstage) = -1 Then
            Dim stailsStatue = Nothing

            For Each stat In statueList
                If stat.name = "seventailsstatue" Then stailsStatue = stat
            Next

            If stailsStatue Is Nothing Then statueList.Add(New Statue(Game.player1.pos, "seventailsstatue", "You see here a golden statue of a fox"))
        End If
    End Sub
    Public Sub writeFloorToFile()
        Dim writer As IO.StreamWriter = Nothing
        Try
            IO.File.Delete("floors/" & floorCode & ".flr")
            writer = IO.File.CreateText("floors/" & floorCode & ".flr")
            writer.WriteLine(saveMFloor)
        Catch ex As Exception
            TextEvent.push("Error writing floor " & floorCode & " to file!")
        Finally
            writer.Flush()
            writer.Close()
        End Try
    End Sub
    Public Sub readFloorFromFile(ByVal fCode As String)
        Dim reader As IO.StreamReader = Nothing
        Try
            If IO.File.Exists("floors/" & fCode & ".flr") Then
                reader = IO.File.OpenText("floors/" & fCode & ".flr")
                loadMFloor(reader.ReadLine)
            End If
        Catch ex As Exception
            TextEvent.push("Error reading floor " & floorCode & " from file!")
        Finally
            If Not reader Is Nothing Then reader.Close()
        End Try
    End Sub
End Class

Public Class Room
    Public Shared MAX_DIRECT_CONNECTIONS As Integer = 5

    Public top_left_pos As Point
    Public width, height As Integer
    Public marked As Boolean = False
    Dim exits As List(Of Point) = New List(Of Point)
    Public connectedRooms As List(Of Room) = New List(Of Room)
    Public directConnections As Integer

    Public Sub New(tlp As Point, w As Integer, h As Integer)
        top_left_pos = tlp
        width = w
        height = h
    End Sub
    Public Function getExit() As Point
        Dim p As Point = Nothing
        Dim ct = 0
        While (p = Nothing Or DDUtils.withinOnePlusMinus(p, exits)) And ct < 12
            Select Case Int(Rnd() * 4)
                Case 0
                    p = New Point(top_left_pos.X, top_left_pos.Y + Int(Rnd() * height))
                Case 1
                    p = New Point(top_left_pos.X + width, top_left_pos.Y + Int(Rnd() * height))
                Case 2
                    p = New Point(top_left_pos.X + Int(Rnd() * width), top_left_pos.Y)
                Case Else
                    p = New Point(top_left_pos.X + Int(Rnd() * width), top_left_pos.Y + height)
            End Select
            ct += 1
        End While

        exits.Add(p)
        Return p
    End Function
    Public Sub connect(ByRef r As Room)
        If connectedRooms.Contains(r) Then Exit Sub

        connectedRooms.Add(r)

        For Each subR In r.connectedRooms
            connect(subR)
        Next
    End Sub
    Public Function connectedTo(ByRef r1 As Room, Optional ByRef checkedRooms As List(Of Point) = Nothing)
        If checkedRooms Is Nothing Then checkedRooms = New List(Of Point)()

        If r1.top_left_pos.Equals(top_left_pos) Then Return True

        For Each r2 In connectedRooms
            If r2.top_left_pos.Equals(r1.top_left_pos) Then Return True

            For Each subR2 In r2.connectedRooms
                If Not checkedRooms.Contains(subR2.top_left_pos) Then
                    checkedRooms.Add(subR2.top_left_pos)
                    Return subR2.connectedTo(r1, checkedRooms)
                End If
            Next
        Next

        Return False
    End Function
    Public Overrides Function Equals(obj As Object) As Boolean
        If Not obj.GetType Is GetType(Room) Then Return False

        Return CType(obj, Room).top_left_pos.Equals(top_left_pos)
    End Function
End Class

Public Class Session
    Dim ID As Integer
    Dim playerPos As Point
    Dim beatBoss As Boolean

    Sub New(ByVal i As Integer, ByVal p As Point, ByVal b As Boolean)
        ID = i
        playerPos = p
        beatBoss = b
    End Sub

    Sub load(ByRef f As mFloor)
        f.playerPosition = playerPos
        f.beatBoss = beatBoss
    End Sub

    Function hasID(ByVal sID As Integer) As Boolean
        Return sID = ID
    End Function

    Overrides Function ToString() As String
        Return ID & "~" & playerPos.X & "~" & playerPos.Y & "~" & beatBoss
    End Function
End Class