﻿Public Class Broodmother
    Inherits pForm
    Sub New()
        MyBase.New(1.25, 1.25, 1.75, 2, 0.5, 0.5, "Broodmother", False)
        MyBase.revertPassage = "Your scales slowly disappear into your skin as you slowly turn back into a biped. On the bright side, you are pretty sure you could still breath fire if you really wanted to."
    End Sub
End Class
