﻿Public Class AntiCurseTag
    Inherits Item

    Sub New()
        '|ID Info|
        setName("Anti_Curse_Tag")
        id = 153
        tier = 3

        '|Item Flags|
        usable = True

        '|Stats|
        count = 0
        value = 777

        '|Description|
        setDesc("A small paper tag with instructions to apply it to your equipment." & DDUtils.RNRN &
                       "Using this item will un-do the slut curse on your currently equipped armor." & DDUtils.RNRN &
                       "To remove cursed (unremoveable) equipment, unequip it as usual while at least 1 Anti_Curse_Tag is present in your inventory.  Anti_Curse_Tags are consumed per each cursed equipment unequipped.")
    End Sub

    Overrides Sub use(ByRef p As Player)
        p.perks(perk.slutcurse) = -1

        If Equipment.antiClothingCurse(p) Then
            TextEvent.push("You apply the anti-curse tag to your equipment.  The slut curse is neutralized!")
        ElseIf p.equippedArmor.getCursed(p) Then
            EquipmentDialogBackend.equipArmor(p, "Naked")
        ElseIf p.equippedWeapon.getCursed(p) Then
            EquipmentDialogBackend.equipWeapon(p, "Fists")
        ElseIf p.equippedAcce.getCursed(p) Then
            EquipmentDialogBackend.equipAcce(p, "Nothing")
        ElseIf p.equippedGlasses.getCursed(p) Then
            EquipmentDialogBackend.equipGlasses(p, "Nothing")
        End If

        p.drawPort()
        count -= 1
    End Sub
End Class
