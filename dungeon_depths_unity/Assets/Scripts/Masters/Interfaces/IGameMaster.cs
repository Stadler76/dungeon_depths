﻿public interface IGameMaster
{
    void new_game();
    void load_game(int save_number);
    void save_game(int save_number);
}
