﻿Public Class MaskedMG
    Inherits ShopNPC
    Sub New()
        MyBase.New()

        '|ID Info|
        name = "Magical Girl"
        sName = name
        npc_index = sNPCInd.maskmaggirl

        '|NPC Flags|
        pronoun = "she"
        p_pronoun = "her"
        r_pronoun = "her"
        isShop = True

        '|Inventory|
        inv.setCount("Gem_of_Progress", 1)
        inv.setCount("Gem_of_Sweetness", 1)
        inv.setCount("Gem_of_Flame", 1)
        inv.setCount("Gem_of_Darkness", 1)

        '|Stats|
        health = 1.0
        maxHealth = 99999
        attack = 9999
        defense = 999
        will = 999
        speed = 99
        gold = 99999
        xp_value = (maxHealth + attack + defense + speed) / 4
        sMaxHealth = maxHealth
        sMaxMana = maxMana
        sAttack = attack
        sdefense = defense
        sWill = will
        sSpeed = speed

        '|Images|
        picNormal = ShopNPC.npcLib.atrs(0).getAt(45)
        picPrincess = ShopNPC.npcLib.atrs(0).getAt(47)
        picBunny = ShopNPC.npcLib.atrs(0).getAt(46)
        picArachne = ShopNPC.npcLib.atrs(0).getAt(72)

        picNPC = New List(Of Image)
        picNPC.AddRange({picNormal,
                         ShopNPC.npcLib.atrs(0).getAt(4),
                         ShopNPC.npcLib.atrs(0).getAt(5),
                         picPrincess,
                         picBunny})

        picNPC.AddRange({ShopNPC.npcLib.atrs(0).getAt(48),
                         ShopNPC.npcLib.atrs(0).getAt(59),
                         picArachne,
                         ShopNPC.npcLib.atrs(0).getAt(95),
                         ShopNPC.npcLib.atrs(0).getAt(96)})
    End Sub

    Public Overrides Sub attackCMD(ByRef target As Entity)
        attackSpell(target, "Howitzer of Love", MyBase.getWIL)
    End Sub

    Public Overrides Sub encounter()
        MyBase.encounter()

        If img_index = 7 Then
            If Game.player1.formName.Equals("Arachne") Then
                TextEvent.pushNPCDialog("Arachne or not, there are still dummies out there that need protectin'!")
            Else
                TextEvent.pushNPCDialog("Hey, if I were to roll out a ""Gem Of Spiders"" do you think you'd take the plunge into eight-legged glory?  Well, I've got the next best thing in the meantime!")
            End If
        ElseIf Int(Rnd() * 25) = 0 Then
            img_index = 4
            TextEvent.pushNPCDialog("Hey, like, have you seen a shadowy guy with a hood?  He TOTALLY put some sorta curse on my wand!  It's not like I, uh, wanted to get all, like, ditzy to have some fun or whatever...")
        ElseIf Int(Rnd() * 25) = 0 Then
            img_index = 6
            TextEvent.pushNPCDialog("Hey kid, how'd you like a quick and easy path to power?  I've got just the rock for you if you don't mind a bit of darkness....")
            inv.item("Gem_of_Darkness").value -= 0.8 * inv.item("Gem_of_Darkness").value
        Else
            img_index = 0
            TextEvent.pushNPCDialog("Always a pleasure to run across another Magic Girl!  What can I get ya?")
        End If

        If img_index = getArachneImageInd() And Not Game.player1.formName.Equals("Arachne") Then inv.setCount(244, 1) Else inv.setCount(244, 0)

        Game.picNPC.BackgroundImage = picNPC(img_index)
    End Sub

    Public Overrides Function toFight() As String
        badForYou()
        Return ""
    End Function
    Public Overrides Function hitBySpell() As String
        badForYou()
        Return ""
    End Function

    Sub badForYou()
        If Game.combat_engaged Then Game.fromCombat()
        Game.picNPC.BackgroundImage = picPrincess
        Game.picNPC.Location = New Point(82 * Game.Size.Width / 1024, 179 * Game.Size.Width / 1024)
        Game.picNPC.Visible = True
        If Game.shop_npc_engaged Then Game.hideNPCButtons()
        TextEvent.pushNPCDialog("Woah there, rookie, don't do anything crazy now.  I'm going to give you some space for now, but if you keep pushing me you're gonna regret it...", AddressOf leave)
    End Sub
    Sub leave()
        Game.player1.pos = Game.currFloor.randPoint()
        isDead = True
        Game.leaveNPC()
    End Sub

    Public Overrides Function getArachneImageInd() As Integer
        Return 7
    End Function
    Public Overrides Function getCatgirlImageInd() As Integer
        Return 8
    End Function
    Public Overrides Function getTrilobiteImageInd() As Integer
        Return 9
    End Function

    Public Overrides Sub toDoll()
        TextEvent.pushNPCDialog("*squeek*")
        Game.picNPC.BackgroundImage = picNPC(5)

        discount = 0.5
    End Sub
End Class
